﻿using UnityEngine;
using System.Collections;

public class FangserScript1 : MonoBehaviour {





	// Properties of descending ship before fangser talks.
	public float strifing = 0;
	public Transform cam;
	public float zoomSpeed = 0.5f;
	private bool isFangserDoneTalking;
	private bool isFangserEntered;

	// Countdown before Fangser's ship moves.
	public float startTime = 0;
	public float endTime = 0;
	public float shipSpeed = 1;
	private float scale;
	private float volume;
	public WarpholeAnimation warphole;
	public ChangeCamera change;

	// Fangser's voice.
	public AudioSource fangserVoice;
	public AudioSource booster;





	void Start () {

		isFangserDoneTalking = false;
		isFangserEntered = false;
		scale = transform.localScale.x;
		volume = 2;

		if (shipSpeed < 0) {
		
			shipSpeed = shipSpeed * -1;

		}

	}





	void FixedUpdate () {

		// Warphole will shrink once Fangser entered.
		if(isFangserEntered) {

			warphole.ShrinkWarp (gameObject);

		}

		// Descend and talk event
		if (transform.position.x < 0) {
		
			// Descend first.
			transform.Translate (strifing, 0, shipSpeed);
			change.To (2);

			// Fangser's ship will set to a perfect center at X.
			if (transform.position.x >= 0) {
			
				transform.position = new Vector3 (0, 0, transform.position.z);

			}

		} else {
		
			// When centered, Fangser's ship will keep moving forward until stop at that point.
			if (!isFangserDoneTalking) {
				
				if (transform.position.z <= -9) {
				
					transform.Translate (0, 0, shipSpeed);

				} else {
				
					change.To (3);

				}

			}

		}

		// Move ship.
		if (startTime > endTime) {

			// Descend first.
			if (isFangserDoneTalking && !fangserVoice.isPlaying) {

				transform.Translate (0, 0, 0.1f * 16);

				if (!booster.isPlaying) {
				
					booster.Play ();
					change.To (0);

				} else if (booster.isPlaying) {
				
					if (volume > 0) {
					
						volume -= 0.01f;

						if (volume <= 0) {
						
							volume = 0;

						}

					}

					booster.volume = volume;

				}

			} else if(!isFangserDoneTalking) {

				// Fangser will talk at that schedule.
				isFangserDoneTalking = true;
				fangserVoice.Play ();
				change.To (1);

			}

		} else {
		
			// Countdown to Move
			startTime += Time.deltaTime;

		}
	
	}





	void OnTriggerEnter(Collider col) {

		shipSpeed = 0;
		transform.position = new Vector3 (0, 0, 20f);

	}





	void OnTriggerStay(Collider col) {

		if (scale > 0) {
		
			scale -= 0.05f;

			if (scale <= 0) {
			
				scale = 0;
				isFangserEntered = true;

			}

		}

		transform.position = new Vector3 (0, 0, 20f);
		transform.localScale = new Vector3 (scale, scale, scale);

	}





}
