﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class CharacterInput {

	public string[] names;
	public string[] messages;

}





public class HereReply : MonoBehaviour {





	// Countdown before Fangser's ship moves.
	public float startTime = 0;
	public float endTime = 0;
	private bool isReplied;

	// Hero's voice.
	public AudioSource voice;
	public AudioClip[] characters;

	// Hero's script.
	public Text nameLabel;
	public Text messageLabel;
	public CharacterInput dialogLabel;
	public Image profilePic;
	public Sprite[] images;
	public ChangeCamera camera;





	void Start () {

		// Replied no encountered yet until specific schedule is hit.
		isReplied = false;

		// Set dialogue for character.
		profilePic.sprite = images[Ship.index];
		nameLabel.text = dialogLabel.names[Ship.index];
		messageLabel.text = dialogLabel.messages[Ship.index];
	
	}





	void FixedUpdate () {

		if (startTime > endTime) {

			// Descend first.
			if (!isReplied) {

				voice.clip = characters [Ship.index];
				voice.Play ();
				isReplied = true;

			}

		} else {
		
			startTime += Time.deltaTime;

		}
	
	}





}
