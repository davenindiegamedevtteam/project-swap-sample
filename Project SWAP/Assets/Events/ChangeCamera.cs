﻿using UnityEngine;
using System.Collections;

public class ChangeCamera : MonoBehaviour {





	public GameObject[] cameras;
	private int currentIndex;





	void Start () {

		currentIndex = 0;

		cameras [0].SetActive (true);

		for (int i = 1; i < cameras.Length; i++) {
		
			cameras [i].SetActive (false);

		}

	}





	public void To(int index) {

		// If you input with the same index, effects canceled.
		if(index == currentIndex) {

			return;

		}
	
		// Previous camera will hide. next camera chosen will set to true.
		cameras [index].SetActive (true);
		cameras [currentIndex].SetActive (false);

		// Reset your current camera index here.
		currentIndex = index;

	}





	public void Next() {

		// Set next index.
		if(currentIndex < cameras.Length) {

			currentIndex++;

			if(currentIndex < cameras.Length) {

				currentIndex = 0;
				cameras [0].SetActive (true);
				cameras [cameras.Length - 1].SetActive (false);

				return;

			}

		}

		// Previous camera will hide. next camera chosen will set to true.
		cameras [currentIndex].SetActive (true);
		cameras [currentIndex - 1].SetActive (false);

	}





	public void Prev() {

		// Set next index.
		if(currentIndex > 0) {

			currentIndex--;

			if(currentIndex <= 0) {

				currentIndex = cameras.Length - 1;
				cameras [currentIndex].SetActive (true);
				cameras [0].SetActive (false);

				return;

			}

		}

		// Previous camera will hide. next camera chosen will set to true.
		cameras [currentIndex].SetActive (true);
		cameras [currentIndex + 1].SetActive (false);

	}





}
