﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class StopPoint {

	public bool enable = false;
	public bool toBottom = false;
	public bool toBehind = false;
	public float y, z;

}





public class AutoZoomIn : MonoBehaviour {





	public GameObject followObject;
	public string followObjectByTag;
	public float startTime = 0;
	public float endTime = 0;
	public float zoomSpeed = 1;
	public float elevationSpeed = -0.05f;

	public StopPoint stopPoint;
	private bool stop;





	void Start () {

		stop = false;

	}





	void FixedUpdate () {

		startTime += Time.deltaTime;
	
		if (startTime > endTime) {
		
			if (followObject != null) {

//				if (transform.position.y > followObject.transform.position.y) {
//
//					transform.Translate (0, -0.01f, zoomSpeed);
//
//				} else if (transform.position.y < followObject.transform.position.y) {
//
//					transform.Translate (0, 0.01f, zoomSpeed);
//
//				}

				transform.LookAt (followObject.transform);
				if(!stop) transform.Translate (0, elevationSpeed, zoomSpeed);

			} else {

				try {

					if (GameObject.FindWithTag (followObjectByTag) != null) {

						transform.LookAt (GameObject.FindWithTag (followObjectByTag).transform);

					}

				} catch(UnityException e) {
				

				}

				if(!stop) transform.Translate (0, elevationSpeed, zoomSpeed);

			}

			if (stopPoint.enable) {
			
				if ((transform.position.y < stopPoint.y) && stopPoint.toBottom) {
				
					stop = true;

				}

				if ((transform.position.y > stopPoint.y) && !stopPoint.toBottom) {

					stop = true;

				}

				if ((transform.position.z < stopPoint.z) && stopPoint.toBehind) {

					stop = true;

				}

				if ((transform.position.z > stopPoint.z) && !stopPoint.toBehind) {

					stop = true;

				}

			}

		}

	}





}
