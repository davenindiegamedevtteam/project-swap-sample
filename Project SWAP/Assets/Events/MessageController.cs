﻿using UnityEngine;
using System.Collections;

public class MessageController : MonoBehaviour {





	// Message Dialog Properties
	float x, y;
	public float speed = 0.05f;

	// Countdown 'til a dialog appears.
	public float startTime = 0;
	public float endTime = 0;
	public AudioSource voice;
	private bool isShown;





	void Start () {

		x = 0;
		y = 0;
		isShown = false;

		transform.localScale = new Vector3 (1, 0, 1);
	
	}





	void FixedUpdate () {

		startTime += Time.deltaTime;

		if (voice != null) {
		
			if (startTime > endTime) {

				if (!isShown && voice.isPlaying) {

					if (y < 1) {

						x += speed*2;
						y += speed;

						if (y >= 1) {

							x = 1;
							y = 1;

							isShown = true;

						}

					}

				} else if (isShown && !voice.isPlaying) {

					if (y > 0) {

						x -= speed*2;
						y -= speed;

						if (y <= 0) {

							x = 0;
							y = 0;

						}

					}

				}

			}

		}

		transform.localScale = new Vector3 (x, y, 1);
	
	}





}
