﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class CountDownToNextScene : MonoBehaviour {





	// Event Properties
	public float startTime = 0;
	public float endTime = 0;
	private bool isReplied;
	public string sceneName = ""; // --> In case if you're not using touch to switch scene and in fade enabled.
	public bool enableFade = false;
	public bool enableIntroFade = false; // --> enable this when new scene is loaded.
	private bool isSkipped;
	private bool startFading;
	public float fadeSpeed = 0.05f;

	// Image Background Properties for Fading
	public Image fadeScreenImage;
	public float r = 0,
				 g = 0,
				 b = 0;
	private float a;

	// Misc
	public GameObject loading; // --> For the text only.
	public GameObject loadingScreen;





	void Start() {
		
		startFading = false;
		isSkipped = false;
		a = 1;

		if (fadeScreenImage != null) {

			if (!enableIntroFade) {

				a = 0;

				if (loadingScreen != null) {

					loadingScreen.SetActive (false);

				}

			} else if(enableIntroFade) {

				if (loadingScreen != null) {

					loadingScreen.SetActive (true);

				}

			}

		}

	}





	void FixedUpdate() {

		// Fade intro in action.
		if (enableIntroFade) {

			if (a > 0) {

				a -= fadeSpeed;

			} else {

				a = 0;
				enableIntroFade = false;

				if ((loading != null) && (loading.activeSelf)) {

					loading.SetActive (false);

					if ((loadingScreen != null) && (loadingScreen.activeSelf)) {

						loadingScreen.SetActive (false);

					}

				}

			}

		} else if (startFading) {

			if (a < 1) {

				a += fadeSpeed;

			} else {

				a = 1;

				if (isSkipped) {

					SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
				
					return;

				}

				if ((sceneName != "") && (sceneName != null) && (startTime > (endTime + 5)) && !isSkipped) {

					SceneManager.LoadScene (sceneName, LoadSceneMode.Single);

				}

			}

		} else {

			if (loadingScreen != null) {

				loadingScreen.SetActive (false);

			}

		}

		// Time runs.
		startTime += Time.deltaTime;;

		// Scene ends within no. of minutes.
		if (startTime > endTime) {
		
			Time.timeScale = 1; // --> Just in case if you're in pause menu.
			startFading = true;

			if (loadingScreen != null) {

				loading.SetActive (true);
				loadingScreen.SetActive (true);

			}

		}

		// User will touch anything to skip to next scene.
		if (a == 0) {

			if (Input.anyKeyDown) {

				Time.timeScale = 1; // --> Just in case if you're in pause menu.

				if (loading != null) loading.SetActive (true);
				if (loadingScreen != null) loadingScreen.SetActive (true);

				if (enableFade && (fadeScreenImage != null)) {

					startFading = true;

					if (loadingScreen != null) {

						loadingScreen.SetActive (true);
						isSkipped = true;

					}

				}

			}

		}

		// Fade foreground.
		if(fadeScreenImage != null) {

			fadeScreenImage.color = new Color (r, g, b, a);

		}

	}





}
