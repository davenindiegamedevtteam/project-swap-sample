﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TransitionTimer : MonoBehaviour {





	// Time Properties
	public float secondsBeforeLoadingScene = 1;
	public float startTime = 0;
	public float endTime = 0;
	public string targetSceneName = "";
	public Text message;





	void Start () {

		endTime = secondsBeforeLoadingScene + Time.time;

		if (message != null) message.text = " ";
	
	}





	void Update () {

		// Run timer.
		startTime = Time.time;

		// Load scene after n second(s).
		if(startTime > endTime) {

			SceneManager.LoadScene (targetSceneName, LoadSceneMode.Single);
			endTime = secondsBeforeLoadingScene + Time.time;

			if (message != null) message.text = "NOW LOADING... ";

		}
	
	}





}
