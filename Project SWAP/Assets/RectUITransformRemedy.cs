﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class Position {

	public float left = 0;
	public float right = 0;
	public float top = 0;
	public float bottom = 0;
	public float posZ = 0;

}





[System.Serializable]
public class Anchors {

	public Vector2 min;
	public Vector2 max;

}





/**
 * 
 * Intended only if the Rect Transform suddenly disfigured
 * when this scene is opened.
 * 
 */
public class RectUITransformRemedy : MonoBehaviour {





	public Position position;
	public Anchors anchors;
	public Vector2 pivot;
	public Vector3 rotation;
	public Vector3 scale;





	void Start () {

//		GetComponent<RectTransform> ().rect.Set
		GetComponent<RectTransform> ().anchorMin = anchors.min;
		GetComponent<RectTransform> ().anchorMax = anchors.max;
	
	}





	void Update () {
	
	}





}
