﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FontBlinker : MonoBehaviour {





	public Text text;
	public float fadeRate = 0;
	private float alpha;
	private bool isReverse;





	void Start () {

		alpha = 0;
		isReverse = false;
	
	}





	void Update () {

		text.color = new Color (1, 1, 1, alpha);

		if (isReverse) {
		
			alpha -= fadeRate;

			if (alpha <= 0) {
			
				alpha = 0;
				isReverse = false;

			}

		} else if (!isReverse) {
		
			alpha += fadeRate;

			if (alpha >= 1) {

				alpha = 1;
				isReverse = true;

			}

		}
	
	}





}
