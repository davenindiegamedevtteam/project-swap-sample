﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShipBoundary : MonoBehaviour {





	public bool showDebugLog = false;
	public Text stat;





	void OnTriggerEnter(Collider col) {
	
		if (col.gameObject.CompareTag ("Enemy Ship") || col.gameObject.CompareTag ("Bullet") || col.gameObject.CompareTag ("Item")) {

			if (showDebugLog) {
			
				if (col.gameObject.CompareTag ("Enemy Ship")) {

					Ship.EnemyShipEscapeCount++;
					print ("[ SHIP BOUNDARY ] + Ships escaped: " + Ship.EnemyShipEscapeCount);

				} else if (col.gameObject.CompareTag ("Bullet")) {
				
					Ship.EnemyBulletCount++;
					print ("[ SHIP BOUNDARY ] + Bullets missed: " + Ship.EnemyBulletCount);

				}

			} else {
			
				if (col.gameObject.CompareTag ("Enemy Ship")) {

					Ship.EnemyShipEscapeCount++;

				} else if (col.gameObject.CompareTag ("Bullet")) {

					Ship.EnemyBulletCount++;

				} else if (col.gameObject.CompareTag ("Item")) {

					Ship.ItemsMissedCount++;

				}

				if (stat != null) {
				
					stat.text = "Enemy Escaped: " + Ship.EnemyShipEscapeCount +
								"\nBullets Dodged: " + Ship.EnemyBulletCount +
								"\nItems missed: " + Ship.ItemsMissedCount;

				}

			}
		
			Destroy (col.gameObject);

		}

	}





}
