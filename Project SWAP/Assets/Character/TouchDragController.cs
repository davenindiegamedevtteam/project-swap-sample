using UnityEngine;
using System.Collections;

/**
 * 
 * This object class allows you to manipulate camera scrolling, useful for
 * 3rd-person side scrolling or 1st-person view in the game.
 * 
 * "Touch Drag Controller Ver. 2" created by David C. Dimalanta.
 * 
 */
public class TouchDragController : MonoBehaviour {
	
	
	
	

	public float distance = 10.0f;
	public bool resetPositionAfterDragging = false;
	public bool dragShip = false;
	public bool enableDebugLog = false;

	public float steerSpeed;
	public float touchAnchorY = 1.6f;
	private Vector3 initialPos;
	private Quaternion initialRotation;
	
	
	
	
	
	void Start() {
		
		// At the start, copy the original starting position of the game object.
		initialPos = transform.position;
		initialRotation = transform.rotation;
		
	}
	
	
	
	
	
	void Update() {
		
		if(Input.touchCount == 0) {

			if (Input.GetMouseButton (0)) { // --> If click-n-dragged by a mouse...
				
				if (!SwitchShip.IsSwitchingShip () &&
					!PauseEvent.isGamePaused() &&
					!ShipBehavior.IsShipDestroyed()) {
				
					dragView (false);

				}
				
			} else if (resetPositionAfterDragging) { // --> Resets the position when the user released the LMB. Available if this boolean is enabled.

				transform.position = initialPos;
				transform.rotation = initialRotation;
				dragShip = false;
				
			} else {
			
				dragShip = false;

				// Speed goes slo-mo as long as the ship is set on the battlefield.
//				if (!SwitchShip.IsSwitchingShip () && !ShipBehavior.IsShipDestroyed () && ShipBehavior.IsShipEntered ()) {
//				
//					Time.timeScale = 0.25f;
//
//				} else {
//				
//					Time.timeScale = 1;
//
//				}

			}
			
		} else if(Input.touchCount == 1) { // --> If touch-n-dragged onto the touchscreen...

			if ((Input.GetTouch (0).phase == TouchPhase.Moved) || (Input.GetTouch (0).phase == TouchPhase.Stationary)) {
				
				if (!SwitchShip.IsSwitchingShip () &&
					!PauseEvent.isGamePaused() &&
					!ShipBehavior.IsShipDestroyed()) {

					dragView (false);

				}
				
			} else if (resetPositionAfterDragging) { // Reset the position when the finger lifted up. Available if this boolean is enabled.

				transform.position = initialPos;
				transform.rotation = initialRotation;
				dragShip = false;
				
			} else {
			
				dragShip = false;

				// Speed goes slo-mo as long as the ship is set on the battlefield.
//				if (!SwitchShip.IsSwitchingShip () && !ShipBehavior.IsShipDestroyed () && ShipBehavior.IsShipEntered ()) {
//
//					Time.timeScale = 0.25f;
//
//				} else {
//
//					Time.timeScale = 1;
//
//				}

			}
		}
		
	}
	
	
	
	
	
	/**
	 * 
	 * Uses a raycast to drag the camera view.
	 * 
	 */
	private void dragView(bool isTouched) {
		
		// Set the ray pointer.
		Ray ray;

		// Initialize ray pointer depending on the gaming platform.
		if(isTouched) {

			ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

		} else {

			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		}

		// Set and update position movement.
		RaycastHit hit;

		// See if if you touched a ship.
		if (Physics.Raycast (ray, out hit, 100)) {

			// If reached near ship's butt, this will prevent from shaking when stopped.
			if (hit.collider.gameObject.CompareTag ("Player Ship Butt")) {

				dragShip = false;
				Roll.DisableRoll(true);

			} else {
			
				dragShip = true;
				Roll.DisableRoll(false);

			}

		} else {

			dragShip = true;
			Roll.DisableRoll(false);

		}

		// Ship auto-follow directly to the finger's coordinates.
		if(!SwitchShip.IsSwitchingShip () &&
		   !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() &&
		   !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(0) &&
			dragShip) {

			if ((GameObject.FindGameObjectWithTag ("Player Ship").transform.position.z - touchAnchorY) < ray.GetPoint (distance).z) {
			
				if (enableDebugLog) print ("Behind ship, Z: " + ray.GetPoint (distance).z);
//				GameObject.FindGameObjectWithTag ("Player Ship Butt").transform.LookAt (ray.GetPoint (distance));

				// To smoothen the left/right steering...
				float x = 0;
				float z = steerSpeed;

				if (GameObject.FindGameObjectWithTag ("Player Ship").transform.position.x > ray.GetPoint (distance).x) {

					x = steerSpeed * -1;

				} else if (GameObject.FindGameObjectWithTag ("Player Ship").transform.position.x < ray.GetPoint (distance).x) {

					x = steerSpeed * 1;

				}

				// Steer and ensures no shaking when draggin 'til stop point.
				if ((GameObject.FindGameObjectWithTag ("Player Ship").transform.position.z - touchAnchorY) >= ray.GetPoint (distance).z) {

					GameObject.FindGameObjectWithTag ("Player Ship").transform.Translate (x, 0, 0);

				} else {
				
					GameObject.FindGameObjectWithTag ("Player Ship").transform.Translate (x, 0, z);

				}

			} else if ((GameObject.FindGameObjectWithTag ("Player Ship").transform.position.z - touchAnchorY) > ray.GetPoint (distance).z) {

				if (enableDebugLog) print ("In front of ship Z: " + ray.GetPoint (distance).z);
//				GameObject.FindGameObjectWithTag ("Player Ship Butt").transform.LookAt (-ray.GetPoint (distance));

				// To smoothen the left/right steering...
				float x = 0;
				float z = steerSpeed;

				if (GameObject.FindGameObjectWithTag ("Player Ship").transform.position.x > ray.GetPoint (distance).x) {

					x = steerSpeed * -1;

				} else if (GameObject.FindGameObjectWithTag ("Player Ship").transform.position.x < ray.GetPoint (distance).x) {

					x = steerSpeed * 1;

				}

				// Steer and ensures no shaking when draggin 'til stop point.
				if (GameObject.FindGameObjectWithTag ("Player Ship").transform.position.z <= ray.GetPoint (distance).z) {

					GameObject.FindGameObjectWithTag ("Player Ship").transform.Translate (x, 0, 0);

				} else {

					GameObject.FindGameObjectWithTag ("Player Ship").transform.Translate (x, 0, -z);

				}

			}

			// Checking for swipe direction...
			CheckSwipeDirection (ray.GetPoint (distance).x, ray.GetPoint (distance).y);

		}

		// If it is a ship, drag it.
//		if (dragShip && !ShipBehavior.IsShipDestroyed ()) {
//		
////			transform.position = ray.GetPoint (distance);
//			transform.position = new Vector3 (transform.position.x, 0, GameObject.FindGameObjectWithTag ("Player Ship Butt").transform.localPosition.z);
//			transform.rotation = Quaternion.Euler (Vector3.zero); // --> For some reason, it will ensure stability of the game object ship.
//
//			// Set to normal speed.
////			Time.timeScale = 1;
//
//		}

//		Debug.Log ("Mouse Coordinate: (" + transform.position.x + ", " + transform.position.y + ")");
		
	}





	private void CheckSwipeDirection(float deltaX, float deltaY) {

		float delta = (deltaX > deltaY) ? deltaX : deltaY;
		int direction = (int) Mathf.Sign (delta);

		if (direction > 0) {

			Ship.direction = "right";

		} else if (direction < 0) {

			Ship.direction = "left";

		}

	}
	
	
	
	
	
}