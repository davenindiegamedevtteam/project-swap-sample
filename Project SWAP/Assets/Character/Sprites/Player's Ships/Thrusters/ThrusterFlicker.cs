﻿using UnityEngine;
using System.Collections;

public class ThrusterFlicker : MonoBehaviour {





	public float flickerSpeed = 0.1f;
	public float scale;
	private float baseScale;





	void Start () {

		baseScale = transform.localScale.x;
		scale = baseScale;
	
	}





	void FixedUpdate () {

		if (Input.anyKey) {

			if (scale > baseScale) {

				scale -= flickerSpeed;

				if (scale <= baseScale) {

					scale = baseScale * 1.5f;

				}

			} else {
			
				scale = baseScale * 1.5f;

			}

		} else if (Input.touchCount > 0) {

			if (scale > baseScale) {

				scale -= flickerSpeed;

				if (scale <= baseScale) {

					scale = baseScale * 1.5f;

				}

			} else {
			
				scale = baseScale * 1.5f;

			}

		} else {

			if (scale > baseScale) {

				scale -= flickerSpeed;

				if (scale <= baseScale) {

					scale = baseScale;

				}

			}

		}

		transform.localScale = new Vector3 (scale, scale, scale);
	
	}





}
