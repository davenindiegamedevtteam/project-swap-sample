﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileController : MonoBehaviour {





	private float r, g, b, a;
	private bool flip;
	public bool enableColorToShip = true; // --> Button color relatively to selected ship.
	public float fadeSpeed = 0.05f;
	public float minAlpha = 0;
	public Image profileImage;
	public Sprite[] profiles;





	void Start () {

		r = 1;
		g = 1;
		b = 1;
		a = 0.8f;
		flip = false;

	}





	void Update () {

		if (!ShipBehavior.IsShipDestroyed () && ShipBehavior.IsShipEntered ()) {
		
			UpdateProfile ();

		}

		if (enableColorToShip) {

			if (Ship.GetName () == "Double Eagle") {

				r = 1;
				g = 0;
				b = 0;

			} else if (Ship.GetName () == "Owl Buster") {

				r = 1;
				g = 0.75f;
				b = 0.25f;

			} else if (Ship.GetName () == "Thunder Valkyrie") {

				r = 1;
				g = 0;
				b = 0.75f;

			} else if (Ship.GetName () == "Blast Hawk") {

				r = 0;
				g = 0.65f;
				b = 1;

			}

		}

		if (flip) {

			if (a > minAlpha) {

				a -= fadeSpeed;

				if (a <= minAlpha) {

					flip = false;
					a = minAlpha;

				}

			}

		} else if (!flip) {

			if (a < 1) {

				a += fadeSpeed;

				if (a >= 1) {

					flip = true;
					a = 1;

				}

			}

		}

		profileImage.color = new Color (r, g, b, a);

	}





	private void UpdateProfile() {
	
		// Everytime when a player change ship, it will identify if this ship came from a main or not.
		if (SwitchShip.IsWingman()) {
		
			// Changing profile pic according to player ship index...
			if (Ship.GetWingmanName () == "Double Eagle") {

				profileImage.sprite = profiles [0];

			} else if (Ship.GetWingmanName () == "Owl Buster") {

				profileImage.sprite = profiles [1];

			} else if (Ship.GetWingmanName () == "Thunder Valkyrie") {

				profileImage.sprite = profiles [2];

			} else if (Ship.GetWingmanName () == "Blast Hawk") {

				profileImage.sprite = profiles [3];

			}

		} else {
		
			// Changing profile pic according to player ship index...
			if (Ship.GetName () == "Double Eagle") {

				profileImage.sprite = profiles [0];

			} else if (Ship.GetName () == "Owl Buster") {

				profileImage.sprite = profiles [1];

			} else if (Ship.GetName () == "Thunder Valkyrie") {

				profileImage.sprite = profiles [2];

			} else if (Ship.GetName () == "Blast Hawk") {

				profileImage.sprite = profiles [3];

			}

		}

	}





}
