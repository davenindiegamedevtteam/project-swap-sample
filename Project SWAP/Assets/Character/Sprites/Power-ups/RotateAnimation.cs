﻿using UnityEngine;
using System.Collections;

public class RotateAnimation : MonoBehaviour {





	// Set rotation axis speed.
	public float x = 0;
	public float y = 0;
	public float z = 0;

	// Material color setup properties
	public float r, g, b, a;
	public bool enableGlow = false;
	private bool rev;





	void Start() {
	
		a = 1;
		rev = false;

	}





	void Update () {

		gameObject.transform.Rotate (x, y, z);

		if (enableGlow) {
		
			if (!rev) {
			
				if (a > 0) {
				
					a -= 0.02f;

					if (a <= 0) {
					
						a = 0;
						rev = true;

					}

				}

			} else if (rev) {

				if (a < 1) {

					a += 0.02f;

					if (a >= 1) {

						a = 1;
						rev = false;

					}

				}

			}

			GetComponent<MeshRenderer> ().material.EnableKeyword ("_EMISSION");
			GetComponent<MeshRenderer> ().material.SetColor("_EmissionColor", new Color (r - a, g - a, b - a, 1));

		}
	
	}





}
