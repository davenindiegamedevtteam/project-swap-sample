﻿using UnityEngine;
using System.Collections;

public class ItemBehavior : MonoBehaviour {





	// Item Behavior Properties
	public float speed = 1;
	public float diagonalSpeed = 0;
	public GameObject aura;

	// Item Effects Properties
	public float HealAmount = 100;

	// Flags
	public bool enableFollow = false;
	public bool followX = false;
	public bool followY = false;
	public bool isObtained = false;
	public bool showDebugLog = false; 

	// Disappear Time Properties
	public float goneTime = 30;
	public float scale = 1;





	void Start () {

	}





	void FixedUpdate () {

		float x = 0;
		float y = 0;

		// Is enemy following player?
		if(enableFollow && !ShipBehavior.IsShipDestroyed()) {

			if (followX) {

				x = transform.position.x;

			}

			if (followY) {

				transform.position = new Vector3 (transform.position.x, 
												  TouchMovement.GetShipPosition ().y, 
												  transform.position.z);

			}

		}

		// Tells if the item is obtained.
		if (isObtained) {

			// Stop
			Vector3 movement = new Vector3 (0, 0, 0);
			GetComponent<Rigidbody> ().velocity = movement * 0;

			// Run timer!
			goneTime -= Time.deltaTime;
			scale -= Time.deltaTime * 1.5f;

			// Shrink the ship.
			if (scale > 0) {

				transform.localScale = new Vector3 (scale, scale, scale);

			} else {

				transform.localScale = new Vector3 (0, 0, 0);

				// Aura drawn when the item is obtained.
				if (goneTime <= 0) {

					Instantiate (aura, transform.position, aura.transform.rotation);
					Destroy (GetComponent<BoxCollider> ());
					Destroy (gameObject);

				}

			}

		} else {
		
//			transform.Translate (x + (diagonalSpeed * 1), y, -speed);

			Vector3 movement = new Vector3 (x + (diagonalSpeed * 1), 0, -Mathf.Sign(speed));
			GetComponent<Rigidbody> ().velocity = movement * speed;

		}

	}





	void OnBecameInvisible() {

		// Item is off the screen.
		Destroy (gameObject);

	}





	void OnTriggerEnter(Collider col) {

		// If item is intacted by player's ship...
		if ((col.gameObject.name == Ship.GetName()) || (col.gameObject.name == Ship.GetWingmanName())) {

			// Status
			if(showDebugLog) {

				print ("[ ITEM BEHAVIOR ] --> Ship received power-up: " + col.gameObject.name);
				print ("[ ITEM BEHAVIOR ] --> Item submerged from: " + gameObject.name);

			}

			// Calculate heal taken.
			ShipBehavior[] ship = col.gameObject.GetComponents<ShipBehavior> ();
			ship [0].RestoreHealth (HealAmount);
			isObtained = true;
			enableFollow = true;
			followX = true;

		}

	}





	void OnTriggerStay(Collider col) {

		// Power-up item follows ship.
		if ((col.gameObject.name == Ship.GetName()) || (col.gameObject.name == Ship.GetWingmanName())) {

			transform.position = col.gameObject.transform.position;

		}

	}





}
