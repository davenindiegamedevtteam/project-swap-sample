﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {





	private static string ship = "Player Ship";
	private static string wingmanShip = "Player Ship";
	public static string direction = "fixed";
	private static float speed = 0.09f;
	private static float wingmanSpeed = 0.09f;
	public static int index = 0; // --> Ship Index
	public static int wingmanIndex = 2;

	public static int EnemyBulletCount;
	public static int EnemyShipEscapeCount;
	public static int ItemsMissedCount;





	public static void SetName(string name) {
	
		ship = name;

	}





	public static void SetWingmanName(string name) {

		wingmanShip = name;

	}





	public static string GetName() {
	
		return ship;

	}





	public static string GetWingmanName() {

		return wingmanShip;

	}





	public static void SetSpeed(float value) {

		speed = value;

	}





	public static void SetWingmanSpeed(float value) {

		wingmanSpeed = value;

	}





	public static float GetSpeed() {

		return speed;

	}





	public static float GetWingmanSpeed() {

		return wingmanSpeed;

	}





}
