﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerShipSpawner : MonoBehaviour {





	// Spawn Time Properties
	public float spawnRate = 1;
	public float startTime = 0;
	public float endTime = 0;
	public bool enableSpawn = true;
	public bool stopAtStartPoint = true;
	private bool isWingmanSpawned;
	public bool enableWingman = true;

	// Player's Ship Game Object
	public GameObject[] ships;
	private GameObject yourShip;
	public GameObject healthBar;
	public AutoZoomIn cam;
	public Text armor;
	public Image profile;
	public AudioSource alarm;
	public AudioSource death;
	public AudioClip[] characterLowHealthVoices;
	public AudioClip[] characterDeathVoices;





	void Start() {

		// Check for sounds available.
		if (death != null) {
		
			alarm.clip = characterLowHealthVoices[Ship.index];
			death.clip = characterDeathVoices [Ship.index];

		}

		// Hit ratio starts calculating.
		HitRate.Reset();

		// Destroy count reseting...
		Statistics.ResetDestroyCount();
		isWingmanSpawned = false;

	}





	void Update () {

		// Switch sounds dynamically.
		if (SwitchShip.IsWingman ()) {

			if (death != null) {

				alarm.clip = characterLowHealthVoices [Ship.wingmanIndex];
				death.clip = characterDeathVoices [Ship.wingmanIndex];

			}

		} else {
		
			if (death != null) {

				alarm.clip = characterLowHealthVoices[Ship.index];
				death.clip = characterDeathVoices [Ship.index];

			}

		}
		
		// Run timer.
		startTime = Time.time;

		// Spawn new enemy ship every n second(s).
		if((startTime > endTime) && enableSpawn) {

			int index = Ship.index;
			yourShip = Instantiate (ships [index]) as GameObject;
			yourShip.transform.position = new Vector3 (transform.position.x, 
													   transform.position.y, 
													   transform.position.z);
//			yourShip.transform.rotation = Quaternion.Euler(0, 0, 0); // 30.55
			yourShip.name = Ship.GetName();
			enableSpawn = false;

			ShipBehavior[] behavior = yourShip.GetComponents<ShipBehavior> ();
			behavior [0].armor = armor;
			behavior [0].healthBar = healthBar;
			behavior [0].alarmSound = alarm;
			behavior [0].deathSound = death;
			behavior [0].stopAtStartPoint = stopAtStartPoint;
			behavior [0].profile = profile;

			if (cam != null) {
			
				cam.followObject = yourShip;

			}

			if(enableWingman) SwitchShip.AddNewShip (yourShip);

		}

	}





	public void SpawnWingman() {

		if (!isWingmanSpawned && enableWingman) {
		
			int index = Ship.wingmanIndex;
			yourShip = Instantiate (ships [index]) as GameObject;
			yourShip.transform.position = new Vector3 (transform.position.x, 
													   transform.position.y, 
													   -15.5f);
			yourShip.name = Ship.GetWingmanName();
			enableSpawn = false;

			ShipBehavior[] behavior = yourShip.GetComponents<ShipBehavior> ();
			behavior [0].armor = armor;
			behavior [0].healthBar = healthBar;
			behavior [0].alarmSound = alarm;
			behavior [0].deathSound = death;
			behavior [0].stopAtStartPoint = stopAtStartPoint;
			behavior [0].profile = profile;

			if (cam != null) {

				cam.followObject = yourShip;

			}

			isWingmanSpawned = true; // --> This will prevent spawning a duplicated 3rd ship. Lock in place.

			SwitchShip.AddNewShip (yourShip);

		}

	}





}
