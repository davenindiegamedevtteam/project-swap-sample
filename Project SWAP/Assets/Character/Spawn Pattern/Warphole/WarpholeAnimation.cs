﻿using UnityEngine;
using System.Collections;

public class WarpholeAnimation : MonoBehaviour {





	// Warp Properties
	public AudioSource warpSound;
	private bool isReversed;
	private bool isSoundPlayed;
	public bool isManual = false; // --> Will close the warp manually via program. Useful for cutscenes.
	public float scaleSpeed = 0.05f;
	private float scale;
	private Vector3 scaleBase;

	// Warp duration
	public float startTime = 0;
	public float endTime = 0;
	public float duration = 0;





	void Start () {

		scaleBase = transform.localScale;
		transform.localScale = new Vector3 (0, 0, 0);
		scale = transform.localScale.x;
		isReversed = false;
		isSoundPlayed = false;

		endTime = duration;

		if (!isManual) {
		
			Destroy (GetComponent<BoxCollider>());
			warpSound.Play ();

		}
	
	}





	void FixedUpdate () {

		transform.Rotate (new Vector3(0, 0, 5));

		if (isReversed && !isManual) {

			// Run timer.
			startTime += Time.deltaTime;

			// Spawn new enemy ship every n second(s).
			if(startTime > endTime) {

				if (!isSoundPlayed) {

					warpSound.pitch = 0.75f;
					warpSound.Play ();
					isSoundPlayed = true;

				}

				if (scale > 0) {

					scale -= scaleSpeed;

				} else {

					scale = 0;
					if(warpSound.isPlaying) Destroy (gameObject, 2); // --> When warp is closed, this game object is removed.

				}

			}
		
		} else if (!isReversed) {

			if (isManual) {

				if (scale < 6) {

					scale += 0.01f;

				} else {

					warpSound.Play ();
					scale = 6;
					isReversed = true;

				}

			} else if(!isManual) {
			
				if (scale < scaleBase.x) {

					scale += scaleSpeed;

				} else {

					scale = scaleBase.x;
					isReversed = true;

				}

			}

		}

		transform.localScale = new Vector3(scale, scale, scale);
	
	}





	/**
	 * 
	 * The warp will close every 1/3 of a millisecond. Recommended to
	 * put this method into the Update().
	 * 
	 */
	public void ShrinkWarp(GameObject ship) {

		if (!isSoundPlayed) {

			warpSound.pitch = 0.75f;
			warpSound.Play ();
			isSoundPlayed = true;

		}

		if (scale > 0) {

			scale -= scaleSpeed;

		} else {

			scale = 0;

			if (!warpSound.isPlaying) {
			
				Destroy (ship); // --> When warp is closed, this game object is removed.
				Destroy (gameObject);

			}
		}

	}





}
