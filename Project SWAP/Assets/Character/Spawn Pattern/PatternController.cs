﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary {

	// Spawn Distance
	public float minX = 0,
				 minY = 0,
				 maxX = 0,
				 maxY = 0;

}

public class PatternController : MonoBehaviour {





	// Pattern Objects
	public GameObject[] patterns;
	private GameObject yourPattern;
	private InitialSpace[] space;

	// Flags
	private int index;
	private int maxIndex;
	private int maxCount; // --> Max ship count flag.
	private static int count; // --> Counter for no. of ships spawned.
	public bool randomizePos = false;
	public bool changePattern = false;
	public bool EnableBackAndForth = false;
	public bool isReverse = false;

	// Boundary
	public Boundary boundary;
	private float x = 0,
				  y = 0;

	// Spawn Pattern Pause Time
	public float pauseTime = 0; // --> Via seconds.
	private float t;
	private bool isTimeBreak;





	void Start () {

		InitialSpace[] space = patterns [0].GetComponents<InitialSpace> ();

		index = 0;
		count = 0;
		x = 0;
		y = 0;
		boundary.minX = -space [0].GetMaxX();
		boundary.minY = -space [0].GetMaxY();
		maxCount = 3;
		maxIndex = patterns.Length;

		isTimeBreak = false;
		t = pauseTime;
		UpdateSpawner (0);
	
	}





	void FixedUpdate () {

		// Get the space or margin of the spawn pattern.
		space = patterns [index].GetComponents<InitialSpace> ();

		// Either move from left to right or random.
		if (!isTimeBreak) {

			if (randomizePos) {

				// ? ? ?

			} else {

				if (isReverse) {

					SpawnPatternRightToLeft ();

				} else {

					SpawnPatternLeftToRight ();

				}

			}

		} else {
		
			pauseTime -= Time.deltaTime;

			if (pauseTime < 0) {
			

				isTimeBreak = false;
				yourPattern.SetActive (true);
				pauseTime = t;

			}

		}

		// Update pattern spawner.
		yourPattern.transform.position = new Vector3(x, y, transform.position.z);
	
	}





	private void UpdateSpawner(int childIndex) {

		yourPattern = Instantiate(patterns [childIndex]) as GameObject;
		isTimeBreak = true;
		yourPattern.SetActive (false);

	}





	private void SpawnPatternLeftToRight() {
	
		// See if X reaches the last point.
		if (x < boundary.maxX) {

			// Increase by amount of X.
			if(count >= maxCount) {

				x += space [0].GetX ();
				count = 0;

			}

			// If reaches end, change pattern index if enabled.
			if (x >= boundary.maxX) {

				if (changePattern) {

					// Change next pattern.
					if (index < maxIndex) {

						// Index check when reaches last index.
						index++;
						if (index == maxIndex) index = 0;

						// Changing pattern to...
						Destroy (yourPattern);
						UpdateSpawner (index);

						// Min-max distance update.
						space = patterns [index].GetComponents<InitialSpace> ();
						boundary.minX = -space [0].GetMaxX ();
						boundary.maxX = space [0].GetMaxX ();
						boundary.minY = -space [0].GetMaxY ();
						boundary.maxY = space [0].GetMaxY ();

					}

				}

				// Check if back-n-forth status checked.
				if(EnableBackAndForth) isReverse = true;
				else x = boundary.minX;

			}

			// For the Y...
			if (y < boundary.maxY) {

				y += space [0].GetY ();
				if (y == boundary.maxY)
					y = 0;

			} else {

				y = boundary.minY;

			}

		}

	}





	private void SpawnPatternRightToLeft() {

		// See if X reaches the last point.
		if (x > boundary.minX) {

			// Increase by amount of X.
			if(count >= maxCount) {

				x -= space [0].GetX ();
				count = 0;

			}

			// If reaches end, change pattern index if enabled.
			if (x <= boundary.minX) {

				if (changePattern) {

					// Change next pattern.
					if (index < maxIndex) {

						// Index check when reaches last index.
						index++;
						if (index == maxIndex) index = 0;

						// Changing pattern to...
						Destroy (yourPattern);
						UpdateSpawner (index);

						// Min-max distance update.
						space = patterns [index].GetComponents<InitialSpace> ();
						boundary.minX = -space [0].GetMaxX ();
						boundary.maxX = space [0].GetMaxX ();
						boundary.minY = -space [0].GetMaxY ();
						boundary.maxY = space [0].GetMaxY ();

					}

				}

				// Check if back-n-forth status checked.
				if(EnableBackAndForth) isReverse = false;
				else x = boundary.maxX;

			}

			// For the Y...
			if (y < boundary.maxY) {

				y += space [0].GetY ();
				if (y == boundary.maxY)
					y = 0;

			} else {

				y = boundary.minY;

			}

		}

	}





	public static void Count() {

		count++;

	}





}
