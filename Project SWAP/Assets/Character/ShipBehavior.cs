﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShipBehavior : MonoBehaviour {





	// Ship Behavior Properties
	public float health = 150;
	private static float hp, maxHp; // --> Needed to check if health is low.
	private float maxHealth;
	public float speed = 1;
	public Text armor;
	public Image profile;
	public GameObject healthBar;
	private static Transform t;
	public SpriteRenderer explosionSprite;
	public Sprite explosion;

	// Flags
	public bool setNoDamage = true; // --> For debugging purpose.
	private static bool isDestroyed;
	private static bool isShipStopped; // --> when moving forward 'til entrance. (Start game only.)
	private bool isWarning; // --> Will said once very low health encounter.
	public bool stopAtStartPoint = true;
	public bool followMainCamera = false;

	// Death Time Properties
	public float deathTime = 30;
	public float explosionScale = 1;
	public float alpha = 1;
	public bool blink = false;
	public bool lowHealth = false;

	// SFX
	public AudioSource explosionSound;
	public AudioSource deathSound;
	public AudioSource alarmSound;





	void Start () {

		// For debugging/test only. It starts at 0, needed for counting no. of enemy bullets
		// and ships escaped off-screen.
		Ship.EnemyShipEscapeCount = 0;
		Ship.EnemyBulletCount = 0;
		Ship.ItemsMissedCount = 0;

		// Displaying health bar info...
		if(armor != null) {

			maxHealth = health;
			maxHp = maxHealth;
			armor.text = " HP: " + health + "/" + maxHealth;

		}

		// Flag on.
		isDestroyed = false;
		isWarning = false;
		isShipStopped = false;

		t = transform;

//		transform.rotation = new Quaternion (45, 0, 0, 0);

	}





	void FixedUpdate () {

		hp = health;

		// For main camera, follows only horizontally.
		if(followMainCamera) {

			GameObject.FindGameObjectWithTag("MainCamera").transform.position = new Vector3(transform.position.x * 0.2f, 13.6f, -5.26f);

		}

		// Keep updating player's HP when switched...
		if(armor != null) {

			armor.text = " HP: " + health + "/" + maxHealth;
			healthBar.transform.localScale = new Vector3 ((health/maxHealth), 1, 1);
			UpdateArmorOnColor ();

		}

		// Player's ship moving. Update ship forward halted when switching.
		if (!SwitchShip.IsSwitchingShip ()) {

			if (stopAtStartPoint) {

				if ((transform.position.z < -11.27f) && !isShipStopped) {

					transform.Translate (0, 0, 0.1f);

				} else {

					isShipStopped = true;

				}

			} else {

				transform.Translate (0, 0, 0.1f);

			}

		}

		// Tells if your ship is still alive. Otherwise, stop and die.
		if (health <= 0)  {

			// Music stops...
			if(GameObject.FindGameObjectWithTag("BGM") != null) {

				GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Stop();

			}

			// Alarm halted.
			alarmSound.Stop();
			lowHealth = false;
			armor.color = Color.gray;
			profile.color = Color.black;

			// Run timer!
			deathTime -= 0.25f;
			explosionScale -= 0.1f;

			// Shrink the ship.
			if (explosionScale > 0.25f) {

				transform.localScale = new Vector3 (explosionScale, explosionScale, explosionScale);

			} else {

				float scale = Random.Range (0.1f, 0.6f);
				float alpha = Random.Range (0.2f, 1);
				transform.localScale = new Vector3 (scale, scale, scale);
				transform.Rotate (0, 0, 1);
				explosionSprite.color = new Color (1, 0.45f, 0, alpha);

			}

			// Particle stopped.
			if(deathTime <= 0) {

				Destroy(GetComponent<BoxCollider> ());

				if(!explosionSound.isPlaying) {

					Destroy(gameObject);

				}

			}

		} 

		if (lowHealth) {

			if (!blink) {
			
				armor.color = new Color (alpha, 0, 0);
				profile.color = new Color (alpha, 0, 0);
				alpha -= 0.25f;

				if (alpha <= 0) {
				
					alpha = 0;
					blink = true;

				}

			} else if (blink) {
			
				armor.color = new Color(alpha, 0, 0);
				profile.color = new Color (alpha, 0, 0);
				alpha += 0.25f;

				if (alpha >= 0) {

					alpha = 1;
					blink = false;

				}

			}

		}

	}





	public void ReduceHealth(float damage) {

		if(!setNoDamage) health -= damage;
		armor.text = " HP: " + health + "/" + maxHealth;
		float amount = health / maxHealth;

		if (health <= (maxHealth / 2)) {
		
			if (health <= (maxHealth * 0.1f)) {
			
				lowHealth = true;

				if (!isWarning) {
				
					alarmSound.Play ();
					isWarning = true;

				}

			} else {
			
				armor.color = Color.yellow;

			}

		}

		if (health <= 0) {

			armor.text = " HP: 0/" + maxHealth;
			isDestroyed = true;
			amount = 0;
			Kill ();

		}

		healthBar.transform.localScale = new Vector3 (amount, 1, 1);

	}





	public void RestoreHealth(float amount) {

		// Health must stayed within max health during healing/repairing.
		if (!isDestroyed) {
		
			if (health < maxHealth) {

				health += amount;

				if (health >= maxHealth) {

					health = maxHealth;

				}

			} else {

				health = maxHealth;

			}

		}

		// Sound alarm must be disabled if HP is above 10%.
		if (health <= (maxHealth / 2)) {

			if (health <= (maxHealth * 0.1f)) {

				lowHealth = true;

			} else {

				armor.color = Color.yellow;
				profile.color = new Color (1, 1, 1);
				lowHealth = false;

				if (isWarning) {

					isWarning = false;

				}

			}

		} else {
		
			lowHealth = false;
			armor.color = Color.white;
			profile.color = new Color (1, 1, 1);

			if (isWarning) {

				isWarning = false;

			}

		}

		// Update health bar GUI.
		armor.text = " HP: " + health + "/" + maxHealth;
		healthBar.transform.localScale = new Vector3 ((health/maxHealth), 1, 1);

	}





	/**
	 * 
	 * Change color as status for HP's condition.
	 * 
	 */
	public void UpdateArmorOnColor() {
	
		if (health <= (maxHealth / 2)) {

			if (health <= (maxHealth * 0.1f)) {

				lowHealth = true;
				isWarning = true;

			} else {

				armor.color = Color.yellow;
				profile.color = new Color (1, 1, 1);
				lowHealth = false;

				if (isWarning) {

					isWarning = false;

				}

			}

		} else {

			lowHealth = false;
			armor.color = Color.white;
			profile.color = new Color (1, 1, 1);

			if (isWarning) {

				isWarning = false;

			}

		}

	}





	private void Kill() {

		// Play explosion + death.
		explosionSound.Play ();
		deathSound.Play ();

		// Change ship sprite into explosion.
		explosionSprite.sprite = explosion;
		explosionSprite.color = new Color (1, 0.45f, 0);

	}





	public float GetHealthAmount() {

		return health;

	}





	public static bool IsShipDestroyed() {
	
		return isDestroyed;

	}





	public static bool IsHealthLow() {

		if (hp <= (maxHp * 0.1f)) {
		
			return true;
		
		} else {
		
			return false;

		}

	}





	public static bool IsShipEntered() {
	
		return isShipStopped;

	}





	public static void ShipNotEntered() {
	
		isShipStopped = false;

	}





	public static Vector3 GetShipPosition() {

		return t.position;

	}





}
