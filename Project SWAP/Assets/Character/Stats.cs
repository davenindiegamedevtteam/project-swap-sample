﻿using UnityEngine;
using System.Collections;

public class Stats : MonoBehaviour {





	public ShipSelect ss;
	public GameObject[] statBars; // --> In order for indexes: attack, HP, mobility, and rapid fire.
	public float raiseSpeed = 0.05f;
	private float a, h, m, r;
	private float ma, mh, mm, mr;





	void Start () {

		// Initial stats
		a = 0;
		h = 0;
		m = 0;
		r = 0;

		// Max stats
		ma = 40 / 100; // --> Attack Power
		mh = 40 / 100; // --> Defense Power
		mm = 40 / 100; // --> Mobility Power
		mr = 40 / 100; // --> Firing Rate
	
	}





	void Update () {

		UpdateStats ();
	
	}





	void UpdateStats() {

		if (a < ma) {
		
			a += raiseSpeed;

		} else {
		
			a = ma;

		}

		if (h < mh) {

			h += raiseSpeed;

		} else {

			h = mh;

		}

		if (m < mm) {

			m += raiseSpeed;

		} else {

			m = mm;

		}

		if (r < mr) {

			r += raiseSpeed;

		} else {

			r = mr;

		}

		statBars [0].transform.localScale = new Vector3 (a, 1, 1);
		statBars [1].transform.localScale = new Vector3 (h, 1, 1);
		statBars [2].transform.localScale = new Vector3 (m, 1, 1);
		statBars [3].transform.localScale = new Vector3 (r, 1, 1);

	}





	/**
	 * 
	 * Following indexes to check for ship stats:
	 * 
	 * 0 = Dual Eagle
	 * 1 = Buster Taurus
	 * 2 = Thunder Valkyrie
	 * 3 = Doom Ray
	 * 
	 */
	public void ReupdateStats(int i) {

		// The original computation for stat bar length is:
		//
		// Stat Bar scale = (current_level * 10)/100
		//				  = current_level/10 --> Where 10 is the max level.
		//
		// Since the strongest/highest stat among ship available in the first version
		// is Clobber Dan's Owl Buster, with the attack power of 60/100, the new
		// formula for now will be:
		//
		// Stat bar scale = level/6
	
		// Ship Info
		switch (i) {

		// Dual Eagle's Stats
		case 0:
			ma = 40f / 60f; // --> Attack Power
			mh = 40f / 60f; // --> Defense Power
			mm = 40f / 60f; // --> Mobility Power
			mr = 40f / 60f; // --> Firing Rate
			a = 0;
			h = 0;
			m = 0;
			r = 0;
			break;

		// Owl Buster' Stats
		case 1:
			ma = 60f / 60f; // --> Attack Power
			mh = 60f / 60f; // --> Defense Power
			mm = 20f / 60f; // --> Mobility Power
			mr = 20f / 60f; // --> Firing Rate
			a = 0;
			h = 0;
			m = 0;
			r = 0;
			break;

		// Thunder Valkyrie
		case 2:
			ma = 30f / 60f; // --> Attack Power
			mh = 30f / 60f; // --> Defense Power
			mm = 50f / 60f; // --> Mobility Power
			mr = 50f / 60f; // --> Firing Rate
			a = 0;
			h = 0;
			m = 0;
			r = 0;
			break;

		// Blast Hawk
		case 3:
			ma = 60f / 60f; // --> Attack Power
			mh = 20f / 60f; // --> Defense Power
			mm = 20f / 60f; // --> Mobility Power
			mr = 60f / 60f; // --> Firing Rate
			a = 0;
			h = 0;
			m = 0;
			r = 0;
			break;

		}

	}





}
