﻿using UnityEngine;
using System.Collections;

/**
 * 
 * Ver. 2 Enemy spawner by David C. Dimalanta.
 * 
 */
public class EnemySpawner : MonoBehaviour {





	// Spawn Time Properties
	public float spawnRatePerSecond = 1;
	public int spawnCountDownPerSecond = 0;
	public int pauseTime = 0;
	private int times;

	// Enemy Ship Game Object
	public GameObject[] enemies;
	private GameObject yourEnemy;
	public GameObject warpHole;
	private GameObject yourWarpHole;

	// Misc
	public bool moveSpawnerX = false;
	public bool moveSpawnerY = false;
	public bool countedAsPattern = true;
	public bool enableEmergencySpawn = false; // --> Usually reserved for spawning medic ships.





	void Start() {

		times = 0;

		StartCoroutine (SpawnShip());

	}





	void FixedUpdate () {

		// Initialize X and Y
		float x = 0;
		float y = 0;

		// Ask if the spawner moves randomly.
		if(moveSpawnerX || moveSpawnerY) {

			if(moveSpawnerX) x = Random.Range(-2.5f, 2.5f);
			if(moveSpawnerY) y = Random.Range(-3, 3);

		} else {

			x = transform.position.x;
			y = transform.position.y;

		}

		// Update ship's starting position after spawned.
		transform.position = new Vector3(x, y, transform.position.z);

		// Check emergency trigger.
		if (ShipBehavior.IsHealthLow () && enableEmergencySpawn) {

			times++;

		} else {
		
			times = 0;

		}

		// Spawn medic ship instantly when your ship's HP is at 10%.
		if((times == 1) && enableEmergencySpawn) {

			SpawnMedicShip ();

		}

	}





	IEnumerator SpawnShip() {

		yield return new WaitForSeconds (spawnCountDownPerSecond);

		while (!ShipBehavior.IsShipDestroyed ()) {

			// Pause for a while. This is the spawn rate.
			yield return new WaitForSeconds (spawnRatePerSecond);
		
			// Spawning warp hole first.
			if(warpHole != null) {

				yourWarpHole = Instantiate (warpHole) as GameObject;
				yourWarpHole.transform.position = new Vector3 (transform.position.x, 
					transform.position.y, 
					transform.position.z);
				yourWarpHole.name = "Warphole";

			}

			// Spawning enemies...
			int index = Random.Range (0, enemies.Length);
			yourEnemy = Instantiate (enemies [index]) as GameObject;
			yourEnemy.transform.position = new Vector3 (transform.position.x, 
				transform.position.y, 
				transform.position.z);

			// If this ship is a medic ship, the name will changed.
			if (enableEmergencySpawn) {

				yourEnemy.name = "Medic Ship";

			} else {

				yourEnemy.name = "Enemy Ship";

			}

			// Warphole scale relatively to the enemy's ship.
			if(warpHole != null) {

				yourWarpHole.transform.localScale = yourEnemy.transform.localScale;

			}

			// Time out...
			yield return new WaitForSeconds (pauseTime);

			if(countedAsPattern) PatternController.Count ();

		}

	}





	private void SpawnMedicShip() {

		// Spawning warp hole first.
		if(warpHole != null) {

			yourWarpHole = Instantiate (warpHole) as GameObject;
			yourWarpHole.transform.position = new Vector3 (transform.position.x, 
														   transform.position.y, 
														   transform.position.z);
			yourWarpHole.name = "Warphole";

		}

		// Spawning a medic ship...
		int index = Random.Range (0, enemies.Length);
		yourEnemy = Instantiate (enemies [index]) as GameObject;
		yourEnemy.transform.position = new Vector3 (transform.position.x, 
													transform.position.y, 
													transform.position.z);
		yourEnemy.name = "Medic Ship";

		// Warphole scale relatively to the enemy's ship.
		if(warpHole != null) {

			yourWarpHole.transform.localScale = yourEnemy.transform.localScale;

		}

	}





}
