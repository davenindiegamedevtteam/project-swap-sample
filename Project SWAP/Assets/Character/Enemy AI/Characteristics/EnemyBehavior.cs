﻿using UnityEngine;
using System.Collections;

public class EnemyBehavior : MonoBehaviour {





	// Enemy Behavior Properties
	public float health = 100;
	public float speed = 1;
	public float strafeSpeed = 0;
	public float followSpeed = 1;
	public float knockback = 0.5f; // --> Stun Force Level
	private float stun = 0;
	public float suicideDamage = 90;
	public int scoreWorth = 100;
	public float explodeSize = 3;
	private EnemyProjectileBehavior[] bullets;
	public GameObject[] containedPowerUps;
	private GameObject yourPowerUpItem;

	// Flags
	public bool reverseMovement = true;
	public bool setNoDamage = true; // --> For debugging purpose.
	public bool enableFollow = false;
	public bool followX = false;
	public bool followY = false;
	public bool isDestroyed = false;
	public bool showDebugLog = false;
	private static bool destroyed;

	// Death Time Properties
	public float deathTime = 30;
	public float scale = 1;
	public GameObject explosion;

	// SFX
	public AudioSource hit;





	void Start () {

		bullets = gameObject.GetComponents<EnemyProjectileBehavior> ();

		destroyed = isDestroyed;

	}





	void FixedUpdate () {

		float x = 0;
		float y = 0;

		if(reverseMovement) {

			// Is enemy following player?
			if(enableFollow && !ShipBehavior.IsShipDestroyed()) {

				if (followX) {
				
					if (TouchMovement.GetShipPosition ().x > transform.position.x) {

						x = followSpeed;

					} else if (TouchMovement.GetShipPosition ().x < transform.position.x) {

						x = -followSpeed;

					}

				}

				if (followY && (TouchMovement.GetShipPosition () != null)) {

					transform.position = new Vector3 (transform.position.x, 
													  TouchMovement.GetShipPosition ().y, 
													  transform.position.z);

//					print ("SHIP Y = " + TouchMovement.GetShipPosition ().y + " | ENEMY Y: " + transform.position.y);
				
//					if (GameObject.Find (shipName).transform.position.y > transform.position.y) {
//
//						y = followSpeed;
//
//					} else if (GameObject.Find (shipName).transform.position.y < transform.position.y) {
//
//						y = -followSpeed;
//
//					} else {
//					
////						y = 0;
////						transform.position = new Vector3 (transform.position.x, 
////														  GameObject.Find (shipName).transform.position.y, 
////														  transform.position.z);
//
//					}

				}

			}

			// Tells if the enemy is still alive. Otherwise, stop and die.
			if(health > 0) {

//				transform.Translate (x + (strafeSpeed * 1), y, -speed + stun);

				Vector3 movement = new Vector3 (x + (strafeSpeed * 1), 0, -Mathf.Sign(speed) + stun);
				GetComponent<Rigidbody> ().velocity = movement * speed;
			
			} else {

				// Stop firing!
				if(bullets.Length > 1) bullets[0].StopFiring ();

				// Ship stays in place.
				GetComponent<Rigidbody> ().isKinematic = true;

				// Run timer!
				deathTime -= Time.deltaTime;
				scale -= Time.deltaTime * 5;

				// Shrink the ship.
				if (scale > 0) {

					transform.localScale = new Vector3 (scale, scale, scale);

				} else {
				
					transform.localScale = new Vector3 (0, 0, 0);

					// Ship destroyed. Explosion spawned.
					if(deathTime <= 0) {

						Instantiate (explosion, transform.position, explosion.transform.rotation);
						Destroy(GetComponent<BoxCollider> ());
						Destroy (gameObject);

					}

				}

			}

			// Stun Time
			if(stun > 0) {

				stun -= 0.25f;

			} else {

				stun = 0;

			}

		}

	}





	void OnBecameInvisible() {

		// Enemy escaped.
//		Ship.EnemyShipEscapeCount++;
//		print ("[ ENEMY BEHAVIOR ] --> Ship escaped: " + Ship.EnemyShipEscapeCount);
		Destroy (gameObject);

	}





	void OnTriggerEnter(Collider col) {
	
		// If enemy ship got crashed by player's ship...
		if ((col.gameObject.name == Ship.GetName()) || (col.gameObject.name == Ship.GetWingmanName())) {

			// Status
			if(showDebugLog) {

				print ("[ ENEMY BEHAVIOR ] --> Ship destroyed: " + col.gameObject.name);
				print ("[ ENEMY BEHAVIOR ] --> Enemy ship crashed to: " + gameObject.name);

			}

			// Calculate damage taken.
			ShipBehavior[] ship = col.gameObject.GetComponents<ShipBehavior> ();
			ship [0].ReduceHealth (suicideDamage);

			// enemy ship suicide done.
			health = 0;
			deathTime = 0;
			scale = 0;
			GetComponent<Rigidbody> ().isKinematic = true;
			Kill ();

		}

	}





	public void ReduceHealth(float damage) {

		hit.Play ();

		if(!setNoDamage) health -= damage;
		stun = knockback;

		if ((health <= 0) && !isDestroyed) {

			isDestroyed = true;
			destroyed = isDestroyed;
			Kill ();

			Statistics.RaiseDestroyCount ();

		}

	}





	private void Kill() {
		
		// Update score and earn points.
		Score.SetScore(scoreWorth);

		// Spawn power-ups.
		SpawnPowerUps();

	}





	private void SpawnPowerUps() {
	
		int index = Random.Range (0, containedPowerUps.Length);

		if (containedPowerUps.Length != 0) {

			yourPowerUpItem = Instantiate (containedPowerUps [index]) as GameObject;
			yourPowerUpItem.transform.position = new Vector3 (transform.position.x, 
														transform.position.y, 
														transform.position.z);
			yourPowerUpItem.name = "Item";

		}

	}





	public bool IsShipDestroyed() {

		return isDestroyed;

	}





	public static bool IsShipDestroyedOnSpot() {

		return destroyed;

	}





}
