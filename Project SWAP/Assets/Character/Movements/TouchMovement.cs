﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class TouchMovement : MonoBehaviour {





	// Touch Properties
	Rigidbody shipBody;
	public float speed = 0.05f;
	public float maxDistance = 7;
	private static float distance;
	private static float forward; // --> Needed for the swing ship trick.
	private static float startPoint;
	private static float x;
	private static bool isDeadEnd;

	// Misc
	private Vector3 recoil;
	private static Transform shipPosition;
	public bool showDebugLog = false;





	void Start() {
	
		shipBody = this.GetComponent<Rigidbody> ();
		speed = Ship.GetSpeed();
		recoil = new Vector3 (0, 0, 0);
		shipPosition = transform;
		distance = maxDistance;
		forward = 0;
		isDeadEnd = false;

	}





	void FixedUpdate() {

		// During switching to wingman, ship position update temporarily halted. 
		if(!SwitchShip.IsSwitchingShip()) shipPosition.position = transform.position;

		// Make ship moved via mobile touchscreen joystick.
		if (!ShipBehavior.IsShipDestroyed () && !SwitchShip.IsSwitchingShip()) {

			if (showDebugLog) print ("[TOUCH CONTROLS] --> Joystick function OK (X-AXIS: " + CrossPlatformInputManager.GetAxis ("Horizontal") + ")");

			// Update movements.
			Vector3 moveVec = new Vector3 (CrossPlatformInputManager.GetAxis ("Horizontal"), 0, 0) * speed;	
//			Vector3 moveVec = new Vector3 (Input.mousePosition.x, 0, 0);
			transform.Translate (moveVec.x, 0, 0); // --> Move ship.
			recoil = moveVec;
			x = moveVec.x;

			// For U-motion...
//			if (ShipBehavior.IsShipEntered ()) {
//
//				transform.position = new Vector3 (transform.position.x, 0, forward);
//
//			} else {
//			
//				forward = ShipBehavior.GetShipPosition ().z;
//				startPoint = forward;
//
////				print ("Elevation: " + elevation + " - " + ShipBehavior.GetShipPosition ().z);
//
//			}

			// Ship must remain stayed within the screen!
			LockMovements (true, maxDistance/2, 0, speed);

//			// Check for directions.
//			if (moveVec.x > 0) {
//
//				Ship.direction = "right";
//
//			} else if (moveVec.x < 0) {
//			
//				Ship.direction = "left";
//
//			} else {
//			
//				Ship.direction = "fixed";
//
//			}

		} else {
		
			if(showDebugLog) print("[TOUCH CONTROLS] --> Joystick function not OK");

		}

		// Detecting dead end...
		if ((transform.position.x > ((maxDistance / 2) - 0.1f)) || (transform.position.x < -((maxDistance / 2) - 0.1f))) {

			if(showDebugLog) print ("[TOUCH CONTROLS] --> Move stopped.");
			isDeadEnd = true;

		} else {
		
			if(showDebugLog) print("[TOUCH CONTROLS] --> Move resumed. L - " + -((maxDistance / 2) - 0.1f) + " R - " + ((maxDistance / 2) - 0.1f));

			isDeadEnd = false;

		}

	}





	void OnBecameInvisible() {

		if(transform.position.x <= Vector3.left.x) {

			transform.Translate(new Vector3(recoil.x, 0, 0));

		}

		if(transform.position.x >= Vector3.right.x) {

			transform.Translate(new Vector3(-recoil.x, 0, 0));

		}

		if(transform.position.y >= Vector3.up.y) {

			transform.Translate(new Vector3(0, -recoil.y, 0));

		}

		if(transform.position.y <= Vector3.down.y) {

			transform.Translate(new Vector3(0, recoil.y, 0));

		}

	}





	/**
	 * 
	 * NOTE: Don't set value of max X or Y at negative value!
	 * Same thing for recoil value.
	 * 
	 */
	void LockMovements(bool limit, float maxX, float maxY, float recoil) {

		// If limit set, ship will set on a designated area.
		if(limit) {

			if (transform.position.x <= -maxX) {
				
				transform.Translate (new Vector3 (recoil, 0, 0));

			}

			if(transform.position.x >= maxX) {
				
				transform.Translate(new Vector3(-recoil, 0, 0));

			}

			if(transform.position.y >= maxY + 2000) {

				transform.Translate(new Vector3(0, -recoil, 0));

			}

			if(transform.position.y <= -maxY) {

				transform.Translate(new Vector3(0, recoil, 0));

			}

		}

	}





	public static Vector3 GetShipPosition() {

		if ((shipPosition.position != null) && (shipPosition != null)) {
		
			return shipPosition.position;

		} else {
		
			return new Vector3 (0, 0, 0);

		}

	}





	public static float GetMaxDistance() {
	
		return distance;

	}





	public static void Elevate(float amount) {

//		print ("start point: " + startPoint);
	
		if (amount < 0) {

			amount = amount * -1;

		}

		if (shipPosition.position.z >= startPoint) {
		
			if (shipPosition.position.x < 0) {

				if (Ship.direction == "left") {

					forward += amount;
				
				} else if (Ship.direction == "right") {

					forward -=  amount;
				
				}

			} else {

				if (Ship.direction == "left") {

					forward -= amount;

				} else if (Ship.direction == "right") {

					forward += amount;

				}

			}

//			if (shipPosition.position.x < 0) {
//
//				if (Ship.direction == "left") {
//
//					forward = amount * x;
//
//				} else if (Ship.direction == "right") {
//
//					forward =  amount * x;
//
//				}
//
//			} else {
//
//				if (Ship.direction == "left") {
//
//					forward = amount * x;
//
//				} else if (Ship.direction == "right") {
//
//					forward = amount * x;
//
//				}
//
//			}

		} else {
		
			forward = startPoint;

		}

//		if (elevation < 0.0f) {
//		
//			elevation = 0;
//
//		}

	}





	public static bool IsDeadEnd() {

		return isDeadEnd;

	}





}
