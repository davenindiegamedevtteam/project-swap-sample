﻿using UnityEngine;
using System.Collections;

public class Roll : MonoBehaviour {





	// Main property
	private float roll;
	public float minY = 0.02f;
	public bool showDebugLog = false;
	public bool is3D = false;
	private static bool isHalting;

	// Roll degree adjustments
	public float minDegree = 5;
	public float maxDegree = 30;





	void Start () {

		roll = 0;
		isHalting = false;
	
	}





	void FixedUpdate () {

		if (Input.anyKey && !isHalting) {

			RollShip ();

		} else if ((Input.touchCount >= 1) && !isHalting) {

			if (Input.GetTouch (Input.touchCount - 1).phase == TouchPhase.Moved) {

				RollShip ();
			
			}

		} else {

			if (roll < -1) {

				roll += minDegree;

			} else if (roll > 1) {

				roll -= minDegree;

			} else {

				roll = 0;

			}

			transform.rotation = Quaternion.Euler (0, 0, roll);

		}
	
	}





	public void RollShip() {

		if (Ship.direction == "right") {

			if(roll > -maxDegree) {

				roll -= minDegree;

			}

		} else if (Ship.direction == "left") {

			if(roll < maxDegree) {

				roll += minDegree;

			}

		}

		transform.rotation = Quaternion.Euler (0, 0, roll);
	
//		if (is3D) {
//		
//			if ((TouchMovement.GetShipPosition ().x < 0)) { // --> Ship rotating to the left...
//
//				if (showDebugLog) {
//
//					print ("[ROLL] --> Heading left side... (" + TouchMovement.GetMaxDistance () +
//					" - " + -(TouchMovement.GetMaxDistance () / 2) + " - " +
//					TouchMovement.GetShipPosition ().x);
//
//				}
//
//				if (Ship.direction == "left") {
//
//					// -45
//					if (!TouchMovement.IsDeadEnd ()) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				} else if (Ship.direction == "right") {
//
//					if (roll < 0) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				}
//
//			} else if ((TouchMovement.GetShipPosition ().x > 0)) { // --> Ship rotating to the right...
//
//				if (showDebugLog) {
//
//					print ("[ROLL] --> Heading right side... (" + TouchMovement.GetMaxDistance () +
//					" - " + (TouchMovement.GetMaxDistance () / 2) + " - " +
//					TouchMovement.GetShipPosition ().x);
//
//				}
//
//				if (Ship.direction == "left") {
//
//					if (roll > 0) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				} else if (Ship.direction == "right") {
//
//					// 45
//					if (!TouchMovement.IsDeadEnd ()) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				}
//
//			}
//
//			// Y: -roll
//			// Z = roll
//			transform.rotation = Quaternion.Euler (0, 0, roll);
//
//		} else {
//		
//			if ((TouchMovement.GetShipPosition ().x < 0)) { // --> Ship rotating to the left...
//
//				if (showDebugLog) {
//
//					print ("[ROLL] --> Heading left side... (" + TouchMovement.GetMaxDistance () +
//						" - " + -(TouchMovement.GetMaxDistance () / 2) + " - " +
//						TouchMovement.GetShipPosition ().x);
//
//				}
//
//				if (Ship.direction == "left") {
//
//					// -45
//					if (!TouchMovement.IsDeadEnd ()) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				} else if (Ship.direction == "right") {
//
//					if (roll < 0) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				}
//
//			} else if ((TouchMovement.GetShipPosition ().x > 0)) { // --> Ship rotating to the right...
//
//				if (showDebugLog) {
//
//					print ("[ROLL] --> Heading right side... (" + TouchMovement.GetMaxDistance () +
//						" - " + (TouchMovement.GetMaxDistance () / 2) + " - " +
//						TouchMovement.GetShipPosition ().x);
//
//				}
//
//				if (Ship.direction == "left") {
//
//					if (roll > 0) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				} else if (Ship.direction == "right") {
//
//					// 45
//					if (!TouchMovement.IsDeadEnd ()) {
//
//						roll = TouchMovement.GetShipPosition ().x * minDegree;
//						TouchMovement.Elevate (minY);
//
//					}
//
//				}
//
//			}
//
//			// Y: -roll
//			// Z = roll
//			transform.rotation = Quaternion.Euler (0, 0, roll);
//
//		}

	}





	public static void DisableRoll(bool enable) {

		isHalting = enable;

	}





}
