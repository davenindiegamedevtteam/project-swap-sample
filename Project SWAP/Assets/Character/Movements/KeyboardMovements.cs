﻿using UnityEngine;
using System.Collections;

public class KeyboardMovements : MonoBehaviour {





	public float speed = 0.5f;
	float xSpeed = 0;
	float ySpeed = 0;
	float zSpeed = 0;
	public bool disableControls = true;
	public GameObject audioLocation;





	void FixedUpdate () {

		if (!ShipBehavior.IsShipDestroyed() && !disableControls) {
		
			getKeys ();
			UpdateMovements (true, 3.5f, 0, speed);

		}
	
	}

	void getKeys() {

		// Check max distance in all 4 corners.
		if(Input.GetKey(KeyCode.Space)) {

			print("LEFT-X: " +  Vector3.left.x);
			print("RIGHT-X: " +  Vector3.right.x);
			print("UP-Y: " +  Vector3.up.y);
			print("DOWN-Y: " +  Vector3.down.y);

		}

		// LEFT
		if(Input.GetKey(KeyCode.A)) {

			Ship.direction = "left";
			xSpeed = -speed;
			//transform.Translate(new Vector3(-0.5f, 0, 0));

		}
		
		// RIGHT
		if(Input.GetKey(KeyCode.D)) {

			Ship.direction = "right";
			xSpeed = speed;
			//transform.Translate(new Vector3(0.5f, 0, 0));

			
		}

		// DOWN
		if(Input.GetKey(KeyCode.S)) {
			
			ySpeed = -speed;
			//transform.Translate(new Vector3(-0.5f, 0, 0));
			
		}
		
		// UP
		if(Input.GetKey(KeyCode.W)) {
			
			ySpeed = speed;
			//transform.Translate(new Vector3(0.5f, 0, 0));
			
			
		}

		// Stop movement horizontally.
		if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) {

			xSpeed = 0;
			Ship.direction = "fixed";

		}

		// Stop movement vertically.
		if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S)) {
			
			ySpeed = 0;
			
		}

	}





	/**
	 * 
	 * NOTE: Don't set value of max X or Y at negative value!
	 * Same thing for recoil value.
	 * 
	 */
	void UpdateMovements(bool limit, float maxX, float maxY, float recoil) {

		// Update the movements
		transform.Translate(new Vector3(xSpeed, ySpeed, zSpeed));
//		audioLocation.transform.Translate(new Vector3(xSpeed, ySpeed, zSpeed));

		// If limit set, ship will set on a designated area.
		if(limit) {

			if(transform.position.x <= -maxX) {

				transform.Translate(new Vector3(recoil, 0, 0));
//				audioLocation.transform.Translate(new Vector3(recoil, 0, 0));

			}

			if(transform.position.x >= maxX) {

				transform.Translate(new Vector3(-recoil, 0, 0));
//				audioLocation.transform.Translate(new Vector3(-recoil, 0, 0));

			}

			if(transform.position.y >= maxY + 2000) {

				transform.Translate(new Vector3(0, -recoil, 0));
//				audioLocation.transform.Translate(new Vector3(0, -recoil, 0));

			}

			if(transform.position.y <= -maxY) {

				transform.Translate(new Vector3(0, recoil, 0));
//				audioLocation.transform.Translate(new Vector3(0, recoil, 0));

			}

		}

	}





	void OnBecameInvisible() {

//		if(transform.position.x <= Vector3.left.x) {
//
//			transform.Translate(new Vector3(0.25f, 0, 0));
//
//		}
//
//		if(transform.position.x >= Vector3.right.x) {
//			
//			transform.Translate(new Vector3(-0.25f, 0, 0));
//			
//		}
//
//		if(transform.position.y >= Vector3.up.y) {
//
//			transform.Translate(new Vector3(0, -0.25f, 0));
//
//		}
//
//		if(transform.position.y <= Vector3.down.y) {
//			
//			transform.Translate(new Vector3(0, 0.25f, 0));
//			
//		}
		
	}





}
