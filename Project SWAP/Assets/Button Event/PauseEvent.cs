﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseEvent : MonoBehaviour {





	public GameObject pausePanel;
	public GameObject gamePanel;
	public GameObject gameoverPanel;
	public GameObject JoystickControl;
	public AdScript ads;
	private static bool isPaused = false;
	public bool isFullscreenAdsShown = false;
	public float countDownToGameOver = 0; // --> GAME OVER screen appears within n millisecond(s).





	void Start () {

		isPaused = false;
		isFullscreenAdsShown = false;
		pausePanel.SetActive (false);
		gameoverPanel.SetActive (false);
		gamePanel.SetActive (true);

		// Checking for ads...
		if(CheckNetwork.checkOnlineStatus()) {

			ads.showAds (false);

		}
	
	}





	void Update () {

		pausePanel.SetActive (isPaused);

		if (ShipBehavior.IsShipDestroyed ()) {

			if (countDownToGameOver <= 0) {

				gamePanel.SetActive (false);
				gameoverPanel.SetActive (true);
				JoystickControl.transform.localScale = new Vector3(0, 0, 0);

				// Checking for ads...
				if(CheckNetwork.checkOnlineStatus()) {

					if (ads.getFullscreenAd () != null) {
					
						if(!isFullscreenAdsShown) {

							ads.showAds (true);
							isFullscreenAdsShown = true;

						}

					} else {
					
						ads.RequestInterstitial ();

					}

				}
			
			} else {
			
				countDownToGameOver -= Time.deltaTime;

			} 

		}
	
	}





	public void Freeze() {

		if (!isPaused) {
		
			// All game freeze.
			Time.timeScale = 0;
			isPaused = true;
			gamePanel.SetActive (false);

			// Joystick and fire button hide this way, setting scale all to 0.
			JoystickControl.transform.localScale = new Vector3(0, 0, 0);

			// Hide ads.
			if(CheckNetwork.checkOnlineStatus()) {

				ads.hideBannerView ();

			}

		} else if (isPaused) {

			// Game resumes.
//			Time.timeScale = 0.25f;
			Time.timeScale = 1;
			isPaused = false;
			gamePanel.SetActive (true);

			// Joystick set to visible by rescaling, preventing loss of input calculation.
			JoystickControl.transform.localScale = new Vector3(1, 1, 1);

			// Checking for ads...
			if(CheckNetwork.checkOnlineStatus()) {

				ads.showAds (false);

			}

		}

	}





	public static bool isGamePaused() {

		return isPaused;

	}





}
