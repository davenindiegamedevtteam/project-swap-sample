﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonAnimation : MonoBehaviour {





	public float r, g, b, a;
	private bool flip;
	public bool enableColorToShip = true; // --> Button color relatively to selected ship.
	public bool manualColor = false;
	public float fadeSpeed = 0.05f;
	public float minAlpha = 0;
	public float maxAlpha = 1;
	public Image buttonImage;





	void Start () {

		if (!manualColor) {
		
			r = 1;
			g = 1;
			b = 1;

		}

		a = 0.8f;
		flip = false;
	
	}





	void Update () {

		if (enableColorToShip) {
		
			if (Ship.GetName () == "Double Eagle") {

				r = 1;
				g = 0;
				b = 0;

			} else if (Ship.GetName () == "Owl Buster") {

				r = 1;
				g = 0.75f;
				b = 0.25f;

			} else if (Ship.GetName () == "Thunder Valkyrie") {

				r = 1;
				g = 0;
				b = 0.75f;

			} else if (Ship.GetName () == "Blast Hawk") {

				r = 0;
				g = 0.65f;
				b = 1;

			}

		}

		if (flip) {
		
			if (a > minAlpha) {
			
				a -= fadeSpeed;

				if (a <= minAlpha) {
				
					flip = false;
					a = minAlpha;

				}

			}

		} else if (!flip) {
		
			if (a < maxAlpha) {

				a += fadeSpeed;

				if (a >= maxAlpha) {

					flip = true;
					a = maxAlpha;

				}

			} else {
			
				flip = true;
				a = maxAlpha;

			}

		}

		buttonImage.color = new Color (r, g, b, a);
	
	}





}
