﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class WingmanDialog : MonoBehaviour {





	// For dialog design...
	public GameObject dialogPanel;
	public GameObject bioPanel;
	public Image white;
	public AudioSource confirmSFX;
	public AudioSource cancelSFX;
	public AudioSource announceSFX;
	public BioScript bio;

	// Character Select Button
	public Sprite[] characters;
	public Image[] pilot;
	public Event e; // --> Get the character voices here.

	// Misc
	public float fadeSpeed;
	private float a;
	private int[] wingmanIndex;
	private string[] wingmanName;
	private int tapCount;
	private int prevButton; // --> Tells if player taps button 1, 2, or 3.

	// Menu Panels
	public GameObject[] panels;
	public GameObject[] dialogPanels;
	public GameObject garage;





	void Start () {

		a = 1;
		tapCount = 0;
		wingmanIndex = new int[3];
		wingmanName = new string[4];
		dialogPanel.SetActive (false);
		bioPanel.SetActive (false);
	
	}





	void FixedUpdate () {

		a -= fadeSpeed;
		white.color = new Color(1, 1, 1, a);

		UpdateCharacterPic ();
	
	}





	void UpdateCharacterPic () {

		// LEGEND:
		// 0 = Caroutine
		// 1 = Dan
		// 2 = Mina
		// 3 = Beta
	
		// Button 1 (Between Carrot and Dan)
		if (Ship.index == 0) {

			pilot [0].sprite = characters [1];
			wingmanIndex[0] = 1;
			wingmanName[0] = "Owl Buster";

		} else {
		
			pilot [0].sprite = characters [0];
			wingmanIndex[0] = 0;
			wingmanName[0] = "Double Eagle";

		}

		// Button 2 (Between Mina and Dan)
		if (Ship.index == 2) {

			pilot [1].sprite = characters [1];
			wingmanIndex[1] = 1;
			wingmanName[1] = "Owl Buster";

		} else {

			pilot [1].sprite = characters [2];
			wingmanIndex[1] = 2;
			wingmanName[1] = "Thunder Valkyrie";

		}

		// Button 3 (Between Mina and Beta)
		if (Ship.index == 3) {

			pilot [2].sprite = characters [1]; // --> Redirect to Dan instead since button 2 has Mina already on it.
			wingmanIndex[2] = 1;
			wingmanName[2] = "Owl Buster";

		} else {

			pilot [2].sprite = characters [3];
			wingmanIndex[2] = 3;
			wingmanName[2] = "Blast Hawk";

		}

	}





	public void SelectWingman(int buttonIndex) {

		if (buttonIndex == 0) {

			if (prevButton != 0) {
			
				prevButton = buttonIndex;
				tapCount = 0;

			}
		
			pilot [0].color = Color.white;
			pilot [1].color = new Color (0.25f, 0.25f, 0.25f, 1);
			pilot [2].color = new Color (0.25f, 0.25f, 0.25f, 1);

			tapCount++;

			Ship.wingmanIndex = wingmanIndex [0];
			Ship.SetWingmanName (wingmanName[0]);

		}

		if (buttonIndex == 1) {

			if (prevButton != 1) {

				prevButton = buttonIndex;
				tapCount = 0;

			}

			pilot [1].color = Color.white;
			pilot [0].color = new Color (0.25f, 0.25f, 0.25f, 1);
			pilot [2].color = new Color (0.25f, 0.25f, 0.25f, 1);

			tapCount++;

			Ship.wingmanIndex = wingmanIndex [1];
			Ship.SetWingmanName (wingmanName[1]);

		}

		if (buttonIndex == 2) {

			if (prevButton != 2) {

				prevButton = buttonIndex;
				tapCount = 0;

			}

			pilot [2].color = Color.white;
			pilot [1].color = new Color (0.25f, 0.25f, 0.25f, 1);
			pilot [0].color = new Color (0.25f, 0.25f, 0.25f, 1);

			tapCount++;

			Ship.wingmanIndex = wingmanIndex [2];
			Ship.SetWingmanName (wingmanName[2]);

		}

		// Bio dialog shows up when you tap the same wingman twice to select.
		if (tapCount >= 2) {
		
			bioPanel.SetActive (true);
			confirmSFX.Play ();
			e.PlayWithSelectedWingmanVoiceOnly ();

			BioScript.ResetIntroFade ();
			bio.StartFlippingCard ();

			garage.SetActive(false);

			for (int i = 0; i < dialogPanels.Length; i++) {
			
				dialogPanels [i].SetActive (false);

			}

		} else {
		
			cancelSFX.Play ();

		}

	}





	public void ToWingmanDialog() {
	
		dialogPanel.SetActive (true);
		announceSFX.Play ();
		confirmSFX.Play ();
		a = 1;
		tapCount = 0;
		prevButton = 0;

		for (int i = 0; i < panels.Length; i++) {
					
			panels[i].SetActive (false);
			
		}

//		for (int i = 0; i < GameObject.FindGameObjectsWithTag ("Ship Select").Length; i++) {
//		
//			GameObject.FindGameObjectsWithTag ("Ship Select")[i].SetActive (false);
//
//		}

	}





	public void CancelDialog() {
	
		// Dialog closes.
		dialogPanel.SetActive (false);
		cancelSFX.Play ();

		// Reset color of the button
		pilot [0].color = Color.white;
		pilot [1].color = Color.white;
		pilot [2].color = Color.white;

		// Re-appear ship select dialog.
		for (int i = 0; i < panels.Length; i++) {

			panels[i].SetActive (true);

		}

//		for (int i = 0; i < GameObject.FindGameObjectsWithTag ("Ship Select").Length; i++) {
//
//			GameObject.FindGameObjectsWithTag ("Ship Select")[i].SetActive (true);
//
//		}

	}





	public void CancelBioDialog() {

		// Dialog closes.
		bioPanel.SetActive (false);
		cancelSFX.Play ();

		// Reset color of the button
		pilot [0].color = Color.white;
		pilot [1].color = Color.white;
		pilot [2].color = Color.white;

		// Tap count is reset.
		tapCount = 0;

		// Show Garage
		garage.SetActive(true);

		// Re-show dialog
		for (int i = 0; i < dialogPanels.Length; i++) {

			dialogPanels [i].SetActive (true);

		}

	}





}
