﻿using UnityEngine;
using System.Collections;

public class SwitchShip : MonoBehaviour {


	private static GameObject[] ships;
	private static int index;
	public PlayerShipSpawner spawner;

	private static bool isSwitchedToWingman;
	private static bool isSwitching; // --> Different from the "isSwitchedToWingman," usually changing formation state.


	void Start () {

		// Setting trigger...
		index = 0;
		ships = new GameObject[2];
		isSwitchedToWingman = false;
		isSwitching = false;
	
	}





	void FixedUpdate () {

		// 1st Ship will do exit and 2nd ship do re-enter state. (Vice versa.)
		if (isSwitching) {
		
			SwitchWingman ();

		}

	}





	public static void AddNewShip(GameObject ship) {
	
		ships [index] = ship;
		index++;

	}





	public void ChangeShip() {

		if (!ShipBehavior.IsShipDestroyed () && ShipBehavior.IsShipEntered()) {

			if (!isSwitching) {
			
				isSwitching = true;

			}

		}

	}





	public static bool IsWingman() {
	
		return isSwitchedToWingman;

	}





	public static bool IsSwitchingShip() {
	
		return isSwitching;

	}





	private void SwitchWingman() {

		if (!isSwitchedToWingman) {

			// Spawn first before updating wingman's ship position relative to main ship then hide 1st ship.
			spawner.SpawnWingman (); 

			// Wingman set starting point from main ship's x and z coordinate before moving intro transition.
			if (!ships [1].activeSelf) {

				ships [1].SetActive (true);
				ships [1].transform.position = new Vector3 (ships [0].transform.position.x + 1.1f, 
					ships [0].transform.position.y, 
					-16);
				ships [1].transform.rotation = ships [0].transform.rotation;

			} else {

				// Keep moving strafing to left till stops at last position set.
				ships [1].transform.Translate (-0.01f, 0, 0.1f);

			}

			// Main ship flees.
			ships [0].transform.Translate (-0.05f, 0, -0.1f);

			// Main ship fleed and the controls focused on wingman.
			if (ships [0].transform.position.z < -15) {

				ships [0].SetActive (false);
				isSwitchedToWingman = true;
				ShipBehavior.ShipNotEntered ();
				isSwitching = false;

			}

		} else if (isSwitchedToWingman) {

			// Same thing as before but this time it's main ship's turn to set position for transition.
			if (!ships [0].activeSelf) {

				ships [0].SetActive (true);
				ships [0].transform.position = new Vector3 (ships [1].transform.position.x + 1.1f, 
					ships [1].transform.position.y, 
					-16);
				ships [0].transform.rotation = ships [1].transform.rotation;

			} else {

				// Keep moving strafing to left till stops at last position set.
				ships [0].transform.Translate (-0.01f, 0, 0.1f);

			}

			ships [1].transform.Translate (-0.05f, 0, -0.1f);

			if (ships [1].transform.position.z < -15) {

				ships [1].SetActive (false);
				isSwitchedToWingman = false;
				ShipBehavior.ShipNotEntered ();
				isSwitching = false;

			}

		}

	}





}
