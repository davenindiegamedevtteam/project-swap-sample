﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Event : MonoBehaviour {





	// Event Properties
	public string searchSceneByTouch = null;
	private string sceneName = ""; // --> In case if you're not using touch to switch scene and in fade enabled.
	private bool toggleGrid;
	private bool toggleSpaceBackground;
	public bool touchToNextScene = false;
	public bool enableFade = false;
	public bool enableIntroFade = false; // --> enable this when new scene is loaded.
	private bool startFading;
	private bool isPlayingWithCharacterVoice;
	public bool playAtTheStart = false; // --> Character enters at the game.
	public bool hide = false;
	private static bool isLoadingScene;
	public float fadeSpeed = 0.05f;
	public float pauseTime = 0; // --> In seconds.

	// Character Voices
	public AudioClip[] characterVoices;

	// Image Background Properties for Fading
	public Image fadeScreenImage;
	public float r = 0,
				 g = 0,
				 b = 0;
	private float a;

	// Misc
	public GameObject gridField;
	public GameObject starField;
	public GameObject loading; // --> For the text only.
	public GameObject loadingScreen;
	public AudioSource sound;
	private AudioClip defaultSound;
	public bool isWingman = false;

	// BGM
	public AudioSource bgm;





	void Start() {

		defaultSound = sound.clip;
	
		toggleGrid = false;
		toggleSpaceBackground = false;
		startFading = false;
		isPlayingWithCharacterVoice = false;
		isLoadingScene = false;
		if(gridField != null) gridField.SetActive (toggleGrid);
		if(starField != null) starField.SetActive (toggleSpaceBackground);

		if (fadeScreenImage != null) {

			a = 1;
		
			if (!enableIntroFade) {

				a = 0;

				if (loadingScreen != null) {

					loadingScreen.SetActive (false);

				}
			
			} else if(enableIntroFade) {
			
				if (loadingScreen != null) {

					loadingScreen.SetActive (true);

				}

			}

		}

		if (playAtTheStart) {
		
			sound.clip = characterVoices [Ship.index];
			sound.Play();

		}

	}





	void Update() {

		// Fade intro in action.
		if (enableIntroFade) {

			if (a > 0) {
			
				a -= fadeSpeed;

			} else {
			
				a = 0;
				enableIntroFade = false;

				if ((loading != null) && (loading.activeSelf)) {
				
					loading.SetActive (false);

					if ((loadingScreen != null) && (loadingScreen.activeSelf)) {

						loadingScreen.SetActive (false);

					}
				
				}

				if (hide) {
				
					this.enabled = false;

				}

			}

		} else if (startFading) {
		
			if (a < 1) {

				a += fadeSpeed;

			} else {

				a = 1;

				if (isPlayingWithCharacterVoice) {

					if (!sound.isPlaying) {
					
						if ((searchSceneByTouch != "") && (searchSceneByTouch != null)) {

							SceneManager.LoadScene (searchSceneByTouch, LoadSceneMode.Single);

						} else {

							SceneManager.LoadScene (sceneName, LoadSceneMode.Single);

						}

					}
				
				} else {

					if (pauseTime <= 0) {
					
						if ((searchSceneByTouch != "") && (searchSceneByTouch != null)) {

							SceneManager.LoadScene (searchSceneByTouch, LoadSceneMode.Single);

						} else {

							SceneManager.LoadScene (sceneName, LoadSceneMode.Single);

						}

					} else {

						pauseTime -= Time.deltaTime;

					}
				
				}

			}

		} else {
		
			if (loadingScreen != null) {

				loadingScreen.SetActive (false);

			}

		}

		// User will touch anything to skip to next scene.
		if (touchToNextScene && (a == 0)) {

			if ((Input.anyKeyDown)) {

				// Check if this scene contains a main bgm...
				if(bgm != null) {

					bgm.Stop ();

				}

				Time.timeScale = 1; // --> Just in case if you're in pause menu.
				sound.clip = defaultSound;
				isLoadingScene = true;

				if (sound != null) sound.Play ();
				if (loading != null) loading.SetActive (true);
				if (loadingScreen != null) loadingScreen.SetActive (true);
			
				if (!enableFade) {

					SceneManager.LoadScene (searchSceneByTouch, LoadSceneMode.Single);

				} else if (enableFade && (fadeScreenImage != null)) {

					startFading = true;

					if (loadingScreen != null) {
					
						loadingScreen.SetActive (true);

					}

				}

			}

		}

		// Fade foreground.
		if(fadeScreenImage != null) {

			fadeScreenImage.color = new Color (r, g, b, a);

		}

	}





	public void Restart(string scene) {

		// Check if this scene contains a main bgm...
		if(bgm != null) {

			bgm.Stop ();

		}

		Time.timeScale = 1; // --> Just in case if you're in pause menu.
		sceneName = scene;
		isLoadingScene = true;

		sound.clip = defaultSound;

		if (sound != null) sound.Play();
		if (loading != null) loading.SetActive (true);
		if (loadingScreen != null) loadingScreen.SetActive (true);

		if (enableFade && (fadeScreenImage != null)) {

			startFading = true;

			if (loadingScreen != null) {

				loadingScreen.SetActive (true);

			}

		} else {
		
			SceneManager.LoadScene (scene, LoadSceneMode.Single);

		}

	}





	public void RestartWithCharacterVoices(string scene) {
	
		Time.timeScale = 1; // --> Just in case if you're in pause menu.
		sceneName = scene;
		isPlayingWithCharacterVoice = true;
		isLoadingScene = true;

		if (sound != null) {
		
			if (isWingman) {
			
				sound.PlayOneShot (characterVoices [Ship.wingmanIndex]);

			} else {
			
				sound.PlayOneShot(characterVoices[Ship.index]);

			}

		}

		if (loading != null) loading.SetActive (true);
		if (loadingScreen != null) loadingScreen.SetActive (true);

		if (enableFade && (fadeScreenImage != null)) {

			startFading = true;

			if (loadingScreen != null) {

				loadingScreen.SetActive (true);

			}

		} else {

			SceneManager.LoadScene (scene, LoadSceneMode.Single);

		}

	}





	public void PlayWithSelectedWingmanVoiceOnly() {
	
		sound.PlayOneShot (characterVoices [Ship.wingmanIndex]);

	}





	public void CloseApp() {
	
		Application.Quit ();

	}





	public void ToggleFieldGrid() {
	
		if (toggleGrid) {

			toggleGrid = false;

		} else if (!toggleGrid) {
		
			toggleGrid = true;

		}

		if(gridField != null) gridField.SetActive (toggleGrid);

	}





	public void ToggleSpaceBackground() {

		if (toggleSpaceBackground) {

			toggleSpaceBackground = false;

		} else if (!toggleSpaceBackground) {

			toggleSpaceBackground = true;

		}

		if(starField != null) starField.SetActive (toggleSpaceBackground);

	}





	public static bool isSceneLoading() {
	
		return isLoadingScene;

	}





	public void SetDefaultSound(AudioSource sfx) {
	
		defaultSound = sfx.clip;

	}





	public void ChangeLoadingScreen(GameObject loading) {
	
		loadingScreen = loading;

	}





}
