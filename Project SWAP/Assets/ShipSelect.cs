﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShipSelect : MonoBehaviour {





	// Check ship index.
	private int index;

	// Ship Select Properties
	public Text name;
	public Sprite[] designs;
	public Sprite[] characters;
	public GameObject[] ships;
	public Image blueprint;
	public Image pilot;

	// Animation
	public float fadeSpeed = 0; // --> For blueprints;
	private float r, g, b , a;
	private float mr, mg, mb;

	// Misc
	public AudioSource sound;
	public Stats stats;
	public bool isWingman = false; // --> Activate this if your're selecting a wingman. 





	void Start () {

		index = 0;
		r = 1;
		g = 1;
		b = 1;
		a = 0;

		for (int i = 0; i < designs.Length; i++) {
		
			blueprint.color = new Color (1, 1, 1, 0);
			blueprint.sprite = designs [i];

			pilot.color = new Color (1, 1, 1, 0);
			pilot.sprite = characters [i];
			pilot.fillAmount = 0;

			ships [i].SetActive (false);

		}

		if (isWingman) {
		
			if (Ship.index == 0) {
			
				index = 1;
				ships [0].SetActive (false);
				ships [1].SetActive (true);
				stats.ReupdateStats (index);

			} else {
			
				ships [0].SetActive (true);
				stats.ReupdateStats (index);

			}

		} else {
		
			ships [0].SetActive (true);
			stats.ReupdateStats (index);

		}
	
	}





	void Update () {

		// Alpha Animation on Blueprint
		if(a < 0.75f) a += fadeSpeed;
		blueprint.color = new Color (1, 1, 1, a);
		pilot.color = new Color (r, g, b, a + 0.25f);
		blueprint.sprite = designs [index];
		pilot.sprite = characters [index];
		pilot.fillAmount = a + 0.25f;


		// Ship Info
		if (isWingman) {

			switch (index) {

			case 0:
				name.text = " Pilot Name: Caroutine Sighthawk\n Ship Name: Double Eagle\n Weapon: Dual Machine Cannon \n Firing Mode: Automatic";
				Ship.SetWingmanName ("Double Eagle");
				Ship.wingmanIndex = index;
				SetColor (1, 0, 0);
				break;

			case 1:
				name.text = " Pilot Name: Clobber Dan Munch \n Ship Name: Owl Buster\n Weapon: Linear Cannon \n Firing Mode: Automatic";
				Ship.SetWingmanName ("Owl Buster");
				Ship.wingmanIndex = index;
				SetColor (1, 0.75f, 0.20f);
				break;

			case 2:
				name.text = " Pilot Name: Mina Heartrush\n Ship Name: Thunder Valkyrie\n Weapon: 20mm Gatling Gun \n Firing Mode: Automatic";
				Ship.SetWingmanName ("Thunder Valkyrie");
				Ship.wingmanIndex = index;
				SetColor (1, 0, 0.75f);
				break;

			case 3:
				name.text = " Pilot: Alice \"Beta\" Sighthawk \n Ship Name: Blast Hawk\n Weapon: Plasma Cannon \n Firing Mode: Semi-auto";
				Ship.SetWingmanName ("Blast Hawk");
				Ship.wingmanIndex = index;
				SetColor (0, 0.65f, 1);
				break;

			}

		} else {
		
			switch (index) {

			case 0:
				name.text = " Pilot Name: Caroutine Sighthawk\n Ship Name: Double Eagle\n Weapon: Dual Machine Cannon \n Firing Mode: Automatic";
				Ship.SetName ("Double Eagle");
				Ship.index = index;
				SetColor (1, 0, 0);
				break;

			case 1:
				name.text = " Pilot Name: Clobber Dan Munch \n Ship Name: Owl Buster\n Weapon: Linear Cannon \n Firing Mode: Automatic";
				Ship.SetName ("Owl Buster");
				Ship.index = index;
				SetColor (1, 0.75f, 0.20f);
				break;

			case 2:
				name.text = " Pilot Name: Mina Heartrush\n Ship Name: Thunder Valkyrie\n Weapon: 20mm Gatling Gun \n Firing Mode: Automatic";
				Ship.SetName ("Thunder Valkyrie");
				Ship.index = index;
				SetColor (1, 0, 0.75f);
				break;

			case 3:
				name.text = " Pilot: Alice \"Beta\" Sighthawk \n Ship Name: Blast Hawk\n Weapon: Plasma Cannon \n Firing Mode: Semi-auto";
				Ship.SetName ("Blast Hawk");
				Ship.index = index;
				SetColor (0, 0.65f, 1);
				break;

			}

		}

		ChangeColor ();
	
	}





	private void SetColor(float r, float g, float b) {
	
		mr = r;
		mg = g;
		mb = b;

	}





	private void ChangeColor() {
	
		if (Event.isSceneLoading ()) {
		
			if (r > mr) {

				r -= 0.05f;

				if (r <= mr) {

					r = mr;

				}

			}

			if (g > mg) {

				g -= 0.05f;

				if (g <= mg) {

					g = mg;

				}

			}

			if (b > mb) {

				b -= 0.05f;

				if (b <= mb) {

					b = mb;

				}

			}

		}

	}





	public void Prev() {

		// Trigger sound.
		sound.Play ();
		a = 0;
	
		// Next ship hides. Prev ship shown.
		ships [index].SetActive (false);

		// Check index...
		if (index > -1) {

			index--;

			// If you're in the wingman select screen, you can't select the same ship.
			if (isWingman) {
			
				if (index == Ship.index) {
				
					index--;

				}

			}

			if (index == -1) {

				index = 3;

				// If you choose your last fighter, skip to the next last fighter.
				if(isWingman) {

					if (Ship.index == index) {
					
						index--;

					}

				}

			}

		}

		// Prev ship shown.
		ships [index].SetActive (true);

		// Re-update current ship's stat.
		stats.ReupdateStats (index);

	}





	public void Next() {

		// Trigger sound.
		sound.Play ();
		a = 0;

		// Previous ship hides.
		ships [index].SetActive (false);

		// Check index...
		if (index < 4) {

			index++;

			// If you're in the wingman select screen, you can't select the same ship.
			if (isWingman) {

				if (index == Ship.index) {

					index++;

				}

			}
		
			if (index == 4) {
			
				index = 0;

				// If you choose your first fighter, skip to the next fighter.
				if(isWingman) {

					if (Ship.index == index) {

						index++;

					}

				}

			}

		}

		// Next ship shown.
		ships [index].SetActive (true);

		// Re-update current ship's stat.
		stats.ReupdateStats (index);

	}





}
