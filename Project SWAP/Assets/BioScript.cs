﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class Card {

	// For design...
	public Image pilotProfile;
	public GameObject cardFront;
	public GameObject cardBack;
	public Sprite[] pilots;

	// For card flipping animation trick...
	public float flipSpeed = 0.01f;
	public float wait = 1; // --> In seconds.

}





[System.Serializable]
public class Info {

	public Image background;
	public Text name;
	public Text birthdate;
	public Text mass;
	public Text description;

}





public class BioScript : MonoBehaviour {





	// For images...
	public Card card;

	// For description...
	public Info info;

	// Other misc...
	public Image fade;
	public float fadeSpeed;
	private static float a, x, s;





	void Start () {

		a = 1;
	
	}





	void Update () {

		// Fade forreground to intro.
		a -= fadeSpeed;
		fade.color = new Color(1, 1, 1, a);

		// Update info on each character.
		switch (Ship.wingmanIndex) {

		case 0:
			card.pilotProfile.sprite = card.pilots [0];
			info.background.color = new Color (1, 0, 0, 0.5f);
			info.name.text = "Name: Caroutine Sighthawk";
			info.birthdate.text = "Birth: 12/29/1990";
			info.mass.text = "Height: 5\'8\" Weight: 60lbs";
			info.description.text = "\nThe Top Gun of Chaser Academy- leader of  the team, his Dad is a leading scientist.\n";
			break;

		case 1:
			card.pilotProfile.sprite = card.pilots [1];
			info.background.color = new Color (1, 0.5f, 0, 0.5f);
			info.name.text = "Name: Clobber Dan Munch";
			info.birthdate.text = "Birth: 5/10/1990";
			info.mass.text = "Height: 6\'5\" Weight: 70lbs";
			info.description.text = "\nBest friend of Caroutine, he loves to eat snacks, and always packs a plastic fork in his mouth, ready to make a meal of the enemy.\n";
			break;

		case 2:
			card.pilotProfile.sprite = card.pilots [2];
			info.background.color = new Color (1, 0, 0.75f, 0.5f);
			info.name.text = "Name: Anastasia Heartrush";
			info.birthdate.text = "Birth: 9/31/1990";
			info.mass.text = "Height: 5\'6\" Weight: 60lbs";
			info.description.text = "\nShe's a Russian exchange student, born with a fierce heart, who has a secret crush on Caroutine since their time in college, and will follow him wherever.\n";
			break;

		case 3:
			card.pilotProfile.sprite = card.pilots [3];
			info.background.color = new Color (0.25f, 0.7f, 0.9f, 0.5f);
			info.name.text = "Name: Alice \"Beta\" Sighthawk";
			info.birthdate.text = "Birth: 12/29/1990";
			info.mass.text = "Height: 5\'5\" Weight: 59lbs";
			info.description.text = "\nTwin sister of Caroutine. She loves big guns and even bigger explosions.\n";
			break;

		}

		// Card flips when a player tapped a selected wingman.
		FlipCard();
	
	}





	public void StartFlippingCard() {

		card.cardBack.transform.localScale = new Vector3 (1, 1, 1);
		card.cardFront.transform.localScale = new Vector3 (0, 1, 1);

		card.cardBack.SetActive (true);
		card.cardFront.SetActive (false);

		x = 1;
		s = card.wait;

	}





	private void FlipCard() {

		s -= Time.deltaTime;

		if (s <= 0) {
		
			if (card.cardBack.activeSelf) {

				if (x > 0) {

					x -= card.flipSpeed;

					if (x < 0) {

						card.cardBack.SetActive (false);
						card.cardFront.SetActive (true);

						x = 0;

					}

					card.cardBack.transform.localScale = new Vector3 (x, 1, 1);

				}

			} else if (card.cardFront.activeSelf) {

				if (x < 1) {

					x += card.flipSpeed;

					if (x > 1) {

						x = 1;

					}

					card.cardFront.transform.localScale = new Vector3 (x, 1, 1);

				}

			}

		}

	}





	public static void ResetIntroFade() {

		a = 1;

	}





	public void HideBioDialog() {

		gameObject.SetActive (false);

	}





}
