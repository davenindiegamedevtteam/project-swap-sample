﻿using UnityEngine;
using System.Collections;

public class ExplodeAnimationController : MonoBehaviour {





	public string animationName;
	public Animation anim;





	void Start () {

		if (anim != null) {
		
			anim [animationName].wrapMode = WrapMode.Once;
			anim.Play (animationName);

		}
	
	}





	void Update () {

		if (!GetComponent<AudioSource> ().isPlaying) {
		
			Destroy (gameObject, 0);

		}
	
	}





}
