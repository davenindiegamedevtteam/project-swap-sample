﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/**
 * 
 * Update your points worth regardless the target.
 * 
 */
public class Score : MonoBehaviour {





	public Text score;
	public Text highscore;
	private static int points;





	void Start () {

		points = 0;
	
	}





	void Update () {

		// Update score display.
		score.text = " Score: " + points;
		highscore.text = " Highscore: " + PlayerPrefs.GetInt (DataFile.HIGHSCORE_DATA, 0).ToString ();

		// Check if a player got a new score.
		if(points > PlayerPrefs.GetInt(DataFile.HIGHSCORE_DATA, 0)) {

			PlayerPrefs.SetInt (DataFile.HIGHSCORE_DATA, points);
			highscore.color = Color.red;

		}
	
	}





	public static void SetScore(int pointsEarned) {
	
		points += pointsEarned;

	}





	public static int GetScore() {
	
		return PlayerPrefs.GetInt (DataFile.HIGHSCORE_DATA, 0);

	}





}
