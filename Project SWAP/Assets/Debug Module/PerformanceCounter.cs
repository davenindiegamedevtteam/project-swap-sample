﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Diagnostics;

public class PerformanceCounter : MonoBehaviour {





	public Text CPU;
	public Text RAM;
	public float UpdateRateBySeconds = 1;

	private System.Diagnostics.PerformanceCounter cpuCounter;
	private System.Diagnostics.PerformanceCounter ramCounter;





	void Start () {

		System.Diagnostics.PerformanceCounterCategory.Exists ("PerformanceCounter");

		cpuCounter = new System.Diagnostics.PerformanceCounter ();
		cpuCounter.CategoryName = "Processor";
		cpuCounter.CounterName = "% Processor Time";
		cpuCounter.InstanceName = "_Total";

		ramCounter = new System.Diagnostics.PerformanceCounter ("Memory", "Available MBytes");

		if (UpdateRateBySeconds < 1) {

			UpdateRateBySeconds = 1;

		}

		StartCoroutine (UpdatePerformance());

	}





	void Update () {

	}





	IEnumerator UpdatePerformance() {

		while (true) {

			Process p = Process.GetCurrentProcess ();

			CPU.text = "CPU Process: " + GetCurrentCpuUsage ();
			RAM.text = "RAM Available: " + GetAvailableRam ();

//			CPU.text = "CPU Process: " + GetCurrentCpuUsage ();
//			RAM.text = "RAM Available: " + p.WorkingSet64;

			yield return new WaitForSeconds (UpdateRateBySeconds);

		}

	}





	public string GetCurrentCpuUsage() {

		return cpuCounter.NextValue () + "%";

	}





	public string GetAvailableRam() {

		return ramCounter.NextValue () + "%";

	}





}
