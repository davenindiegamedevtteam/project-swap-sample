﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPS : MonoBehaviour {




	private float deltaTime;
	public Text msg;
	public float UpdateRateBySeconds = 1;





	void Start () {

		deltaTime = 0;

		if (UpdateRateBySeconds < 1) {
		
			UpdateRateBySeconds = 1;

		}

		StartCoroutine (UpdateFPS());
	
	}





	void Update () {

		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
	
	}





	IEnumerator UpdateFPS() {
	
		while (true) {
		
			float msec = deltaTime * 1000.0f;
			float fps = 1.0f / deltaTime;
			string text = string.Format ("{0:0.0} ms ({1:0.0} fps)", msec, fps);
			msg.text = "FPS: " + text;

			yield return new WaitForSeconds (UpdateRateBySeconds);

		}

	}





}
