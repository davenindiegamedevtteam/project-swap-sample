﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CheckNetwork : MonoBehaviour {





	// TODO --> "http://stackoverflow.com/questions/24351155/unity-check-internet-connection-availability"





	public GUIText status;
	public GUIText shade;
	public Text stat; // --> For Unity 5; preview online status with UI text.
	public float seconds = 0;
	public bool setDebug;
	private static bool isOnline;





	void Start () {

		isOnline = false;
		StartCoroutine(CheckConnectionToMasterServer());
	
	}





	public static bool checkOnlineStatus() {
	
		return isOnline;
	
	}





	private IEnumerator CheckConnectionToMasterServer() {

		// Set server ping.
		Ping pingMasterServer = new Ping("8.8.8.8");
		Debug.Log(pingMasterServer.ip);
		float startTime = Time.time;

		// Yield for X seconds
		while (!pingMasterServer.isDone && Time.time < startTime + seconds) {

			if(setDebug) {

				if ((status != null) && (shade != null)) {



				}
			
				if ((status != null) && (shade != null)) {
				
					status.text = "Check connection status...";
					shade.text = status.text;

				}

			}

			yield return new WaitForSeconds(0.1f);

		}

		// Check for connection status.
		if(pingMasterServer.isDone) {

			if(setDebug) {
			
				print("ONLINE");

				if ((status != null) && (shade != null)) {

					status.text = "Connection online.";
					shade.text = status.text;

				} else if (stat != null) {
				
					stat.text = "Connection online";

				}
			
			}

			isOnline = true;

		} else {

			if(setDebug) {

				print("OFFLINE");

				if ((status != null) && (shade != null)) {

					status.text = "Connection offline.";
					shade.text = status.text;

				} else if (stat != null) {

					stat.text = "Connection offine";

				}

			}

			isOnline = false;

		}

		// Once more...
		switch (Application.internetReachability)
		{
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			if(setDebug) {
				
				print("ONLINE (LAN)");

				if ((status != null) && (shade != null)) {

					status.text = "Connection online. (LAN)";
					shade.text = status.text;

				} else if (stat != null) {

					stat.text = "Connection online. (LAN)";

				}
				
			}
			
			isOnline = true;
			break;

		case NetworkReachability.ReachableViaCarrierDataNetwork:
			if(setDebug) {
				
				print("ONLINE (Data Network)");

				if ((status != null) && (shade != null)) {

					status.text = "Connection online. (Data Network)";
					shade.text = status.text;

				} else if (stat != null) {

					stat.text = "Connection online. (Data/WiFi)";

				}
				
			}
			
			isOnline = true;
			break;

		default:
			if(setDebug) {
				
				print("OFFLINE");

				if ((status != null) && (shade != null)) {

					status.text = "Connection offline.";
					shade.text = status.text;

				} else if (stat != null) {

					stat.text = "Connection offine";

				}
				
			}
			
			isOnline = false;
			break;
		}

	}





}
