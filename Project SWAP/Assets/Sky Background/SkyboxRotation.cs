﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SkySphereProperties {

	public Vector3 angle;
	public float rotationSpeed;

}

public class SkyboxRotation : MonoBehaviour {





	public Skybox space;
	public GameObject skySphere; // --> To be only used in customed XYZ rotation.
	public SkySphereProperties skySphereProperties;
	public float rotationSpeed;





	void Start () {

		if (space != null) {

			space.material.SetFloat ("_Rotation", 0);

		}
	
	}





	void FixedUpdate () {

		if (space != null) {
		
			space.material.SetFloat ("_Rotation", Time.time * rotationSpeed);

		}

		if (skySphere != null) {

			skySphere.transform.Rotate (skySphereProperties.angle.x * skySphereProperties.rotationSpeed,
										skySphereProperties.angle.y * skySphereProperties.rotationSpeed,
										skySphereProperties.angle.z * skySphereProperties.rotationSpeed);

		}
	
	}





}
