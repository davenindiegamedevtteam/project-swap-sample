﻿using UnityEngine;
using System.Collections;

public class ScreenOrientationScript : MonoBehaviour {




	/** Type either the following (case sensitive): Portrait, Landscape-left, Landscape-right */ public string position = "Portrait";

	void Start () {

		if (position == "Portrait") {
		
			Screen.orientation = ScreenOrientation.Portrait;

		} else if (position == "Landscape-left") {

			Screen.orientation = ScreenOrientation.LandscapeLeft;

		} else if (position == "Landscape-right") {

			Screen.orientation = ScreenOrientation.LandscapeRight;

		} else {

			Screen.orientation = ScreenOrientation.AutoRotation;

		}
	
	}





}
