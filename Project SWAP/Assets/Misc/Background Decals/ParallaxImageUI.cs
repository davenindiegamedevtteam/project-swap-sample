﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ParallaxImageUI : MonoBehaviour {




	[Tooltip("The transform of the image UI.")]
	public RectTransform form;
	private Rect screenRect;
	private Vector3 lastPos;

	public float speed = -0.2f;





	void Start () {

		lastPos = form.position;
		screenRect = new Rect(0f, 0f, Screen.width, Screen.height);

	}





	void Update () {

		form.Translate (speed, 0, 0);

		Vector3[] objectCorners = new Vector3[4];
		this.GetComponent<RectTransform>().GetWorldCorners(objectCorners);
		bool isObjectOverflowing = false;

		foreach (Vector3 corner in objectCorners) {
			
			if (!screenRect.Contains(corner)) {
				
				isObjectOverflowing = true;
				break;

			}

		}

		if (isObjectOverflowing) form.position = lastPos;

		lastPos = form.position;
	
	}





	void OnBecameInvisible() {
	
		Debug.Log ("Offscreen");
		form.anchoredPosition = new Vector2 (0, 0);

	}





}
