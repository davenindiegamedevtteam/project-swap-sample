﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class AccountManager : MonoBehaviour {





	public Text message;
	public Text logButtonLabel;
	public GameObject dialogBackground;
	public GameObject dialogPanel;
	public GameObject button;
	public AudioSource confirmSound;
	public AudioSource errorSound;
	private float y;





	void Start () {

		y = 0;
		dialogPanel.transform.localScale = new Vector3 (1, 0, 1);

		button.SetActive (false);
		dialogBackground.SetActive (false);
		dialogPanel.SetActive (false);

		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
			.EnableSavedGames()
			.RequireGooglePlus()
			.Build();

		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();

		// Check if log button is sign-in or out.
		if (CheckNetwork.checkOnlineStatus ()) {

			CheckLoginStatus ();

		}
	
	}





	void FixedUpdate () {

		if (y < 1.331115f) {
		
			y += 0.1f;

		} else {
		
			y = 1.331115f;

		}

		dialogPanel.transform.localScale = new Vector3 (1.331115f, y, 1);
	
	}





	public void CheckLoginStatus() {

		// Show dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);

		// If logged-in previously before, this status check and notifies that you are logged-in.
		if (CheckNetwork.checkOnlineStatus ()) {

			if (Social.localUser.authenticated) {

				y = 0;
				message.text = "Your account is currently active.\nYour email:" + ((PlayGamesLocalUser)Social.localUser).Email;
				logButtonLabel.text = "Log-out";
				button.SetActive (true);
				confirmSound.Play ();

			} else {
			
				y = 0;
				message.text = "Please login to load your last current\nprogress saved.";
				logButtonLabel.text = "Log-in via Google";
				button.SetActive (true);
				confirmSound.Play();

			}

		} else {

			message.text = "Connection is lost. Please enable online.";
			button.SetActive (true);

		}

	}





	public void LogInAccount() {

		// Show dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);

		// You can logout if you're logged in.
		if(Social.localUser.authenticated) {

			((PlayGamesPlatform)Social.Active).SignOut ();

			y = 0;
			message.text = "Your account have been logged out.";
			logButtonLabel.text = "Log-in via Google";
			button.SetActive (true);
			confirmSound.Play();

			return;

		} 
	
		// Check for status.
		if (CheckNetwork.checkOnlineStatus ()) {
		
			Social.localUser.Authenticate ((bool success) => {

				if (success) {

					// LOG IN SUCCESS! (Check if the email is identified or not.)
					if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

						y = 0;
						message.text = "Login complete!\nYour email:" + ((PlayGamesLocalUser)Social.localUser).Email;
						logButtonLabel.text = "Log-out";
						button.SetActive (true);
						confirmSound.Play();

					} else {

						y = 0;
						message.text = "Login complete. However, email is not verified or unknown.";
						logButtonLabel.text = "Log-out";
						button.SetActive (true);
						errorSound.Play();

					}

				} else if (!success) {

					// LOG IN FAILED.
					if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

						message.text = "Login failed. Please try again later.\nYour email: ? ? ?";

					} else {

						message.text = "Login failed. Please try again later.\nYour email: " + ((PlayGamesLocalUser)Social.localUser).Email;

					}

					y = 0;
					button.SetActive (true);
					logButtonLabel.text = "Log-in via Google";
					errorSound.Play();

				}

			});

		} else {
		
			message.text = "Connection is lost. Please enable online.";
			button.SetActive (true);

		}

	}





	public void UpdateScore() {

		// Show dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);
	
		// Updating your highscore...
		Social.ReportScore ((long)Score.GetScore (), 
			AchievementReference.leaderboard_best_intergalactic_chasers_top_score,
			(bool success) => {

				if(success) {

					// LEADERBOARD APPEARED!
					if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

						y = 0;
						message.text = "Score updated.";
						button.SetActive (true);
						confirmSound.Play();

					} else {

						y = 0;
						message.text = "Failed to update score...";
						button.SetActive (true);
						errorSound.Play();

					}

				} else {

					y = 0;
					message.text = "Load failed. Please try again later.";
					button.SetActive (true);
					errorSound.Play();

				}

			});

	}





	public void OpenLeaderboard() {

		// Show dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);

		PlayGamesPlatform.Instance.CreateLeaderboard ();

		// Check for status.
		if (Social.localUser.authenticated && CheckNetwork.checkOnlineStatus ()) {

			// Updating your highscore...
			Social.ReportScore ((long)Score.GetScore (), 
				AchievementReference.leaderboard_best_intergalactic_chasers_top_score,
				(bool success) => {

					if(success) {

						// LEADERBOARD APPEARED!
						if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

							dialogPanel.SetActive (false);
							dialogBackground.SetActive (false);
							confirmSound.Play();
							Social.ShowLeaderboardUI();
//							PlayGamesPlatform.Instance.ShowLeaderboardUI(AchievementReference.leaderboard_best_intergalactic_chasers_top_score);
//							((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(AchievementReference.leaderboard_best_intergalactic_chasers_top_score);

						} else {

							y = 0;
							message.text = "Email still not verified. Fix up registration first.\nTry again later.";
							button.SetActive (true);
							errorSound.Play();

						}

					} else {

						y = 0;
						message.text = "Load failed. Please try again later.";
						button.SetActive (true);
						errorSound.Play();

					}

				});
			
		} else if (!Social.localUser.authenticated) {

			y = 0;
			confirmSound.Play();
			button.SetActive (true);
		
			if (!CheckNetwork.checkOnlineStatus ()) {

				message.text = "Connection not available at a moment.";

			} else if (CheckNetwork.checkOnlineStatus ()) {

				message.text = "You must login first in order to see the leaderboard.";

			}

		}

	}





	public void OpenAchievements() {

		// Show dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);

		// Check for status.
		if (Social.localUser.authenticated && CheckNetwork.checkOnlineStatus ()) {

			// ACHIEVEMENTS APPEARED!
			if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

				dialogPanel.SetActive (false);
				dialogBackground.SetActive (false);
				confirmSound.Play();
				((PlayGamesPlatform)Social.Active).ShowAchievementsUI();

			} else {

				y = 0;
				message.text = "Email still not verified. Fix up registration first.\nTry again later.";
				button.SetActive (true);
				errorSound.Play();

			}

		} else if (!Social.localUser.authenticated) {

			if (!CheckNetwork.checkOnlineStatus ()) {

				y = 0;
				confirmSound.Play();
				message.text = "Connection not available at a moment.";
				button.SetActive (true);

			} else if (CheckNetwork.checkOnlineStatus ()) {

				y = 0;
				confirmSound.Play();
				message.text = "You must login first in order to see the achievements.";
				button.SetActive (true);

			}

		}

	}





	public void ShowDialog(string msg) {

		// opens the dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);
		button.SetActive (true);
		message.text = msg;
		confirmSound.Play();

	}





	public void Cancel() {

		// closes the dialog.
		dialogPanel.SetActive (false);
		dialogBackground.SetActive (false);
		button.SetActive (false);
		message.text = "Please wait...";
		confirmSound.Play();

	}





}
