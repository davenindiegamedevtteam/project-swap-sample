﻿using UnityEngine;
using System.Collections;

/**
 * 
 * Intended for retrieving information, useful during
 * load or save state.
 * 
 */
public class DataFile : MonoBehaviour {

	// Record
	public static readonly string HIGHSCORE_DATA = "HI-SCORE";
	public static readonly string SHIPS_DESTROYED = "DESTROYED";
	public static readonly string SHIPS_DESTROYED_PER_GAME = "DESTROYED-PER-GAME";
	public static readonly string HIT_RATE_DATA = "HIT-RATE";

	// Achievement Data. (NOTE: To be set in 0 and 1 as boolean value.)
	public static readonly string ACHIEVEMENT_STATUS_FIRST_TRY = "FIRST_TRY";
	public static readonly string ACHIEVEMENT_STATUS_DESTROY_20 = "DESTROY_20";
	public static readonly string ACHIEVEMENT_STATUS_DESTROY_100 = "DESTROY_100";
	public static readonly string ACHIEVEMENT_STATUS_HIGHSCORE_10000 = "SCORE_10000";
	public static readonly string ACHIEVEMENT_STATUS_HIGHSCORE_50000 = "SCORE_50000";

}
