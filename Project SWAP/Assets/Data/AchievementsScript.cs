﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class AchievementsScript : MonoBehaviour {





	// Flags (1 and 0 as boolean)
	private static int isFirstTime;
	private static int destroy_20;
	private static int destroy_100;
	private static int score_10000;
	private static int score_50000;

	// Dialog
	public Text message;
	public Text title;
	public GameObject dialogBackground;
	public GameObject dialogPanel;
	public GameObject button;
	public AudioSource confirmSound;
	private float y;

	// No. of dialogs to be shown
	public bool enableTestCount = false;
	private bool isShown;
	public int countDialog = 0;
	private static int dialogCount = 0;
	private int count;
	private static ArrayList list;





	#region Mono Main

	void Start () {

		// Setting up dialog count
		list = new ArrayList ();
		count = 0;

		// Setting up dialog position
		y = 0;
		dialogPanel.transform.localScale = new Vector3 (1, 0, 1);
		isShown = false;

		// For dialog count test...
		if (enableTestCount) {

			dialogCount = countDialog;

			for (int i = 0; i < dialogCount; i++) {

				list.Add ("CHECK " + (i + 1));

			}

			ShowDialog ((string)list [0]);

		}

		// Loading all achievements were previously unlocked...
		isFirstTime = PlayerPrefs.GetInt (DataFile.ACHIEVEMENT_STATUS_FIRST_TRY, 0);
		destroy_20 = PlayerPrefs.GetInt (DataFile.ACHIEVEMENT_STATUS_DESTROY_20, 0);
		destroy_100 = PlayerPrefs.GetInt (DataFile.ACHIEVEMENT_STATUS_DESTROY_100, 0);
		score_10000 = PlayerPrefs.GetInt (DataFile.ACHIEVEMENT_STATUS_HIGHSCORE_10000, 0);
		score_50000 = PlayerPrefs.GetInt (DataFile.ACHIEVEMENT_STATUS_HIGHSCORE_50000, 0);


		// Setting up Google Play Games account...
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
													.EnableSavedGames()
													.RequireGooglePlus()
													.Build();
		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();

		// Check if log button is sign-in or out.
		if (CheckNetwork.checkOnlineStatus ()) {

			print ("Progress now loading...");
//			StartCoroutine(LogInAccount());
			LogInAccount();
			button.SetActive (true);

		}
	
	}





	void FixedUpdate () {

		if (y < 1.331115f) {

			y += 0.1f;

		} else {

			y = 1.331115f;

		}

		dialogPanel.transform.localScale = new Vector3 (1, y, 1);

	}

	#endregion





	#region Coroutines

//	IEnumerator LogInAccount() {
	public void LogInAccount() {

//		yield return new WaitForSeconds(3);

		// Check for status.
		if (CheckNetwork.checkOnlineStatus ()) {

			if (Social.localUser.authenticated) {
			
				print ("Google account already online! Checking achievements...");
				ShowDialog ("Google account already loaded.\nWaiting for achievements...");

				// Check for progress.
				CheckIfPlayedForTheFirstTime ();
				UpdateTotalScore ();
				UpdateTargetDestroyCount ();

				return;

			} else {

				Social.localUser.Authenticate ((bool success) => {

					if (success) {

						button.SetActive (true);

						// LOG IN SUCCESS! (Check if the email is identified or not.)
						if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

							print("Google account online!");
							ShowDialog ("Google account loaded.\nWaiting for achievements...");

							// Check for progress.
							CheckIfPlayedForTheFirstTime();
							UpdateTotalScore();
							UpdateTargetDestroyCount();

						} else {

							print("Google account failed to logged-in.");

							ShowDialog ("Updating achievements failed. Please try again.");

						}

					} else if (!success) {

						button.SetActive (true);

						// LOG IN FAILED.
						if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

							print("Login failed. Please try again later.\nYour email: ? ? ?");

						} else {

							print("Login failed. Please try again later.\nYour email: " + ((PlayGamesLocalUser)Social.localUser).Email);

						}

						title.text = "Notice";
						ShowDialog ("Updating achievements failed. Please try again.");

					}

					print ("Progress done loading.");

					return;

				});

			}

			title.text = "Notice";
			ShowDialog ("Google Play Games connection failed. Please try again later.");
			button.SetActive (true);

		} else {

			print("Connection is lost. Please enable online.");

			title.text = "Notice";
			ShowDialog ("Status offline. Please connect your wifi.");

		}

//		yield return new WaitForSeconds(3);

		print ("Progress done loading.");

	}

	#endregion





	#region Button Callbacks

	public void HideDialog(bool setMultiple) {

		print ("Hiding dialog...");

		// closes the dialog.
		if (setMultiple) {

			if (dialogCount > 0) {

				message.text = (string) list[count];
				dialogCount--;
				count++;

				confirmSound.Play();

			} else {
			
				dialogPanel.SetActive (false);
				dialogBackground.SetActive (false);
				button.SetActive (false);
				message.text = "";
				confirmSound.Play();

				count = 0;

				isShown = false;

			}

		} else {
		
			dialogPanel.SetActive (false);
			dialogBackground.SetActive (false);
			button.SetActive (false);
			message.text = "";
			confirmSound.Play();

			isShown = false;

		}

	}





	// This one's hardly necessary to use as button event however.
	public void ShowDialog(string msg) {

		// opens the dialog.
		if(!isShown) {

			print ("Opening dialog...");

			y = 0;
			dialogPanel.SetActive (true);
			dialogBackground.SetActive (true);
			button.SetActive (true);
			message.text = msg;
			confirmSound.Play();

			dialogCount--;
			count++;

			isShown = true;

		}

	}

	#endregion





	#region Progress Check

	public void CheckIfPlayedForTheFirstTime() {
	
		if (isFirstTime == 0) {
		
			isFirstTime = 1;
			dialogCount++;
			PlayerPrefs.SetInt (DataFile.ACHIEVEMENT_STATUS_FIRST_TRY, 1);
			list.Add ("Play for the first time. (Success)");

			print ("Achievement Unlocked! (First gameplay.)");

			RecordAchievement (AchievementReference.achievement_first_space_cadet);

		}
	
	}





	/**
	 * 
	 * To be used in order to track destroy count based on
	 * specific achievements.
	 * 
	 */
	public void UpdateTargetDestroyCount() {

		if (PlayerPrefs.GetInt (DataFile.SHIPS_DESTROYED, 0) >= 20) {

			// Basically, the lowest achievement obtained will go first before...
			if(destroy_20 == 0) {

				dialogCount++;
				list.Add ("20 ships destroyed. (Success)");
				print ("Achievement Unlocked! (Destroy 29.)");

				PlayerPrefs.SetInt (DataFile.ACHIEVEMENT_STATUS_DESTROY_20, 1);
				RecordAchievement (AchievementReference.achievement_ship_wrecker);

			}

			// The next higher achievement will obtain.
			if (PlayerPrefs.GetInt (DataFile.SHIPS_DESTROYED, 0) >= 100) {

				if (destroy_100 == 0) {
				
					dialogCount++;
					list.Add ("100 ships destroyed. (Success)");
					print ("Achievement Unlocked! (Destroy 100 ships.)");

					PlayerPrefs.SetInt (DataFile.ACHIEVEMENT_STATUS_DESTROY_100, 1);
					RecordAchievement (AchievementReference.achievement_ship_destroyer);

				}

			} 

		}

	}





	/**
	 * 
	 * To be used in order to track highscore based on
	 * specific achievements.
	 * 
	 */
	public void UpdateTotalScore() {
	
		if (Score.GetScore () >= 10000) {

			// Same routine, lowest achievement first before...
			if (score_10000 == 0) {
			
				dialogCount++;
				list.Add ("Highscore reached 10,000pts! (Success)");
				print ("Achievement Unlocked! (Reach a highscore of 10,000pts.)");

				PlayerPrefs.SetInt (DataFile.ACHIEVEMENT_STATUS_HIGHSCORE_10000, 1);
				RecordAchievement (AchievementReference.achievement_space_rookie);

			}
		
			// The next one.
			if (Score.GetScore () >= 50000) {

				if (score_50000 == 0) {

					dialogCount++;
					list.Add ("Highscore reached 50,000pts! (Success)");
					print ("Achievement Unlocked! (Reach a highscore of 50,000pts.)");

					PlayerPrefs.SetInt (DataFile.ACHIEVEMENT_STATUS_HIGHSCORE_50000, 1);
					RecordAchievement (AchievementReference.achievement_space_ace);

				}

			}

		}

	}

	#endregion





	#region Record Section

	public void RecordAchievement(string id) {
	
		// Check for status.
		if (CheckNetwork.checkOnlineStatus ()) {

			// Wait mode
			title.text = "Notice";
			ShowDialog ("It will take more time to load. Please wait...");
			button.SetActive (true);

			// Updating your achievements...
			Social.ReportProgress (id, 100f, (bool success) => {

				if (success) {

					// Status is verrified.
					if ((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

						// ? ? ?
						title.text = "Achievement unlocked!";
						ShowDialog ((string)list [0]);

					} else {

						// ? ? ?
						title.text = "Notice";
						ShowDialog ("Updating achievements failed. Please try again.");

					}

				}

			});

		} else {
		
			title.text = "Notice";
			ShowDialog ("Google Play Games connection failed. Please try again later.");

		}

	}

	#endregion





}
