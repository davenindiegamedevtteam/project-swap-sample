using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class AccountManager2 : MonoBehaviour {





	public Text message;
	public Text logButtonLabel;
	public GameObject dialogBackground;
	public GameObject dialogPanel;
	public GameObject button;
	public AudioSource confirmSound;
	public AudioSource errorSound;
	private float y;





	#region PUBLIC_VAR
	public string leaderboard;
    #endregion





	#region DEFAULT_UNITY_CALLBACKS
	void Start () {
		
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate ();

		button.SetActive (false);
		OnLogOut ();

		leaderboard = AchievementReference.leaderboard_best_intergalactic_chasers_top_score;

	}





	void FixedUpdate () {

		if (y < 1.331115f) {

			y += 0.1f;

		} else {

			y = 1.331115f;

		}

		dialogPanel.transform.localScale = new Vector3 (1.331115f, y, 1);

	}





	public void Cancel() {

		// closes the dialog.
		dialogPanel.SetActive (false);
		dialogBackground.SetActive (false);
		button.SetActive (false);
		message.text = "Please wait...";
		confirmSound.Play();

	}
	#endregion





    #region BUTTON_CALLBACKS
	/// <summary>
	/// Login In Into Your Google+ Account
	/// </summary>
	public void LogIn () {

		// Show dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);

		// You can logout if you're logged in.
		if(Social.localUser.authenticated) {

			((PlayGamesPlatform)Social.Active).SignOut ();

			y = 0;
			message.text = "Your account have been logged out.";
			logButtonLabel.text = "Log-in via Google";
			button.SetActive (true);
			confirmSound.Play();

			return;

		} 

		// Check for status.
		Social.localUser.Authenticate ((bool success) => {
			
			if (success) {

				// LOG IN SUCCESS! (Check if the email is identified or not.)
				if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

					y = 0;
					message.text = "Login complete. However, email is unknown.";
					logButtonLabel.text = "Log-out";
					button.SetActive (true);
					errorSound.Play();

				} else {

					y = 0;
					message.text = "Login complete!\nYour email:" + ((PlayGamesLocalUser)Social.localUser).Email;
					logButtonLabel.text = "Log-out";
					button.SetActive (true);
					confirmSound.Play();

				}

			} else if (!success) {

				// LOG IN FAILED.
				if((((PlayGamesLocalUser)Social.localUser).Email != null) || (((PlayGamesLocalUser)Social.localUser).Email != "")) {

					message.text = "Login failed. Please try again later.\nYour email: ? ? ?";

				} else {

					message.text = "Login failed. Please try again later.\nYour email: " + ((PlayGamesLocalUser)Social.localUser).Email;

				}

				y = 0;
				button.SetActive (true);
				logButtonLabel.text = "Log-in via Google";
				errorSound.Play();

			}

		});
	}





	/// <summary>
	/// Shows All Available Leaderborad
	/// </summary>
	public void OnShowLeaderBoard () {

//		OnAddScoreToLeaderBorad ();
//		Social.ShowLeaderboardUI (); // --> Shows all leaderboards.
		confirmSound.Play ();
		errorSound.Play ();
//		((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (leaderboard); // --> Show current (Active) leaderboard.
		PlayGamesPlatform.Instance.ShowLeaderboardUI(AchievementReference.leaderboard_best_intergalactic_chasers_top_score);

	}





	/// <summary>
	/// Adds Score To leader board
	/// </summary>
	public void OnAddScoreToLeaderBorad () {
		
		if (Social.localUser.authenticated) {
			
			Social.ReportScore (100, leaderboard, (bool success) => {
				
				if (success) {

					confirmSound.Play ();
					Debug.Log ("Update Score Success");
					
				} else {

					errorSound.Play ();
					Debug.Log ("Update Score Fail");

				}

			});
		}

	}





	/// <summary>
	/// On Logout of your Google+ Account
	/// </summary>
	public void OnLogOut () {
		
		// Show dialog.
		y = 0;
		dialogPanel.SetActive (true);
		dialogBackground.SetActive (true);

		// You can logout if you're logged in.
		if (Social.localUser.authenticated) {

			((PlayGamesPlatform)Social.Active).SignOut ();

			y = 0;
			message.text = "Your account have been logged out.";
			logButtonLabel.text = "Log-in via Google";
			button.SetActive (true);
			confirmSound.Play ();

		} else {
		
			y = 0;
			message.text = "Already logged out.";
			logButtonLabel.text = "Log-in via Google";
			button.SetActive (true);
			confirmSound.Play ();

		}

	}
    #endregion






}
