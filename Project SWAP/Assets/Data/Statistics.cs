﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Statistics : MonoBehaviour {





	// Labels
	public Text highscoreLabel;
	public Text shipsDestroyedLabel;
	public Text destroyedPerGameLabel;
	public Text hitRateLabel;

	// Ship destroy count
	private static int shipDestroyed;
	private static int shipDestroyedPerGame;





	void Update () {

		highscoreLabel.text = "" + Score.GetScore ();
		shipsDestroyedLabel.text = "" + PlayerPrefs.GetInt (DataFile.SHIPS_DESTROYED, 0);
		destroyedPerGameLabel.text = "" + PlayerPrefs.GetInt (DataFile.SHIPS_DESTROYED_PER_GAME, 0);
		hitRateLabel.text = "" + HitRate.GetResult () + "%";
	
	}





	public static void ResetDestroyCount() {

		shipDestroyed = PlayerPrefs.GetInt (DataFile.SHIPS_DESTROYED, 0);
		shipDestroyedPerGame = 0;

	}





	public static void RaiseDestroyCount() {

		// Make count;
		shipDestroyed++;
		shipDestroyedPerGame++;

		// If old record beats, automatic update on ships destroyed per game.
		if (shipDestroyedPerGame > PlayerPrefs.GetInt (DataFile.SHIPS_DESTROYED_PER_GAME, 0)) {
		
			PlayerPrefs.SetInt (DataFile.SHIPS_DESTROYED_PER_GAME, shipDestroyedPerGame);

		}

		// However, the total overall ships destroyed will permanently counted, even on the next game.
		PlayerPrefs.SetInt (DataFile.SHIPS_DESTROYED, shipDestroyed);

	}





}
