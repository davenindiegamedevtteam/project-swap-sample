using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

/** Sets raycasting and triggers bullet fire when you touched a ship only. */
[System.Serializable]
public class TouchShipProperties {

	public float distance = 10.0f;
	public bool focusRay2D = false;

}

public class ProjectileBehavior : MonoBehaviour {





	// Fire Rate per Second
	public float fireRate = 0;
	public float startFire = 0;
	public float endFire = 0;
	public float shotOffsetX = 0;
	public float shotOffsetY = 0;
	public int shots = 0;

	// Multiple Burst Properties
	public float shotPauseRate = 0;
	private float pauseTimer;
	public int maxShot = 99999;
	public bool enableFire = true;
	public bool enableMultiBurstMode = false;

	// Bullet/Ammo Type
	public FireSoundController sfx;
	public GameObject projectileObject;
	private GameObject yourProjectTile;
	public string firingMode = "automatic";

	// Misc
	public bool showDebugLog = false;

	// For touch-ship only capability to fire...
	public TouchShipProperties touchProperties;





	void Start () {
	
		pauseTimer = 0;

	}





	void Update () {

		if (!ShipBehavior.IsShipDestroyed ()) {
		
			KeyboardButtons ();

		}
	
	}





	void FixedUpdate() {
	
		// Placed it here for touch buttons.
		bool isTriggerPulled = false;

		// Check the type of firing.
		if(firingMode == "automatic") {

			isTriggerPulled = CrossPlatformInputManager.GetButton("Fire");

		} else if(firingMode == "semi-automatic") {

			isTriggerPulled = CrossPlatformInputManager.GetButtonDown("Fire");

		}

		// Check if a player tapped FIRE button.
		if(isTriggerPulled && !ShipBehavior.IsShipDestroyed ()) {

			PullTrigger ();

		}

		// If touched/mouse clicked on a ship to fire instead...
		if (Input.GetMouseButton (0)) {

//			FireByTouchingShip (false);

			PullTrigger ();

		} else if (Input.touchCount >= 1) {

			PullTrigger ();
		
			if(firingMode == "automatic") {

				if ((Input.GetTouch (0).phase == TouchPhase.Moved) || (Input.GetTouch (0).phase == TouchPhase.Stationary)) {
				
//					FireByTouchingShip (true);

				}

			} else if(firingMode == "semi-automatic") {

				if ((Input.GetTouch (0).phase == TouchPhase.Ended) || 
					(Input.GetTouch (0).phase == TouchPhase.Canceled) ||
					(Input.GetTouch (0).phase == TouchPhase.Began)) {

//					FireByTouchingShip (true);

				}

			}

		}

	}





	void KeyboardButtons(){

		if (firingMode == "automatic") {
		
			if (Input.GetKey (KeyCode.J)) {

				PullTrigger ();

			}

		} else if (firingMode == "semi-automatic") {
		
			if (Input.GetKeyDown (KeyCode.J)) {

				PullTrigger ();

			}

		}

	}





	void PullTrigger() {
	
		// Run timer.
		startFire = Time.time;

		// Fire bullet per no. of seconds.
		if(startFire > endFire) {

			// Update hit ratio.
			HitRate.RaiseShotCount();

			// Check if it's in multiple burst state or not.
			if (enableFire) {

				shots += 1;
			
				yourProjectTile = Instantiate(projectileObject) as GameObject;
				yourProjectTile.transform.position = new Vector3(transform.position.x + shotOffsetX,
																 transform.position.y + shotOffsetY,
																 transform.position.z + 0.05f);

				sfx.Play ();

				if ((shots >= maxShot) && enableMultiBurstMode) {
				
					enableFire = false;
					shots = 0;

				}

			}

			endFire = fireRate + Time.time;
			startFire = Time.time;

		}

		// Shot paused in n seconds.
		if(!enableFire) {

			pauseTimer += Time.deltaTime;

			if (pauseTimer > shotPauseRate) {
			
				pauseTimer = 0;
				enableFire = true;

			}

		}

	}





	private void FireByTouchingShip(bool isTouched) {
	
		// Set the ray pointer.
		Ray ray;

		// Initialize ray pointer depending on the gaming platform.
		if(isTouched) {

			ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

		} else {

			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		}

		// Set and update camera position movement.
		RaycastHit hit;

		// See if if you touched a ship.
		if(Physics.Raycast(ray, out hit, 100)) {

			// Check if this is the bottle. It will prevent from autospinning unless you touched a bottle.
			if (hit.collider.gameObject.CompareTag("Player Ship") ||
				hit.collider.gameObject.CompareTag("Player Ship Butt")) {

				PullTrigger ();

			}

		}


	}





}
