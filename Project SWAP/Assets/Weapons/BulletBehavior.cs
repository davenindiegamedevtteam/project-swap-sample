﻿using UnityEngine;
using System.Collections;

public class BulletBehavior : MonoBehaviour {





	// Bullet Properties
	public float velocity = 0;
	public float damage = 30;
	public float shotFadeSpeed = 0.25f; // --> Bullet will fade when the bullet reaches at certain distant range.
	private float r, g, b, alpha; // --> Shot Colors
	public SpriteRenderer bullet;
	public TrailRenderer bulletTrail;
	public bool isHit = false;
	public bool isPenetrable = false;
	public bool showDebugLog = false;
	public bool isReverse = false;
	public bool enemyBullet = false; // --> Counter measure against bug: enemy shot won't hit themselves!
	public bool isPenetrationFailed = false;
	public bool disableShotFade = false;

	// Explosion Mark
	public GameObject explosion;
	private GameObject yourExplosion;





	void Start() {
	
		alpha = 1;

		try {

			r = bullet.color.r;
			g = bullet.color.g;
			b = bullet.color.b;
			
		} catch(UnassignedReferenceException e) {

			if(showDebugLog) print ("[BULLET BEHAVIOR] --> No color found in bullet sprite and/or " +
				   					"trails at the start. Switching to default 1.");

			r = 1;
			g = 1;
			b = 1;
		
		}

	}





	void FixedUpdate () {

		// Move projectile.
		if (isReverse) {
		
			Vector3 movement = new Vector3 (0, 0, -Mathf.Sign(velocity));
			GetComponent<Rigidbody> ().velocity = movement * (velocity / VelocityMultiplier.GetValue());

		} else {

			Vector3 movement = new Vector3 (0, 0, Mathf.Sign(velocity));
			GetComponent<Rigidbody> ().velocity = movement * (velocity / VelocityMultiplier.GetValue());

		}

		// Scrap
		if(isHit) {

			if (!isPenetrable) {
			
				Destroy (gameObject);

			} else if(isPenetrationFailed && isPenetrable) {

				Destroy (gameObject);

			}

		}

		// Fade bullet at certain range distance. (Only player ships.)
		if((transform.position.z >= -0.5f) && !disableShotFade) {

			if (alpha > 0) {
				
				alpha -= shotFadeSpeed;

			} else {
			
				if(showDebugLog) print ("Bullet is out of boundary.");
				Destroy (gameObject);

			}

			try {

				bullet.color = new Color(r, g, b, alpha);

				if(alpha < bulletTrail.startWidth) {

					bulletTrail.startWidth = alpha;

				}

			} catch(UnassignedReferenceException) {
			
				if ((alpha > 0) && showDebugLog) {
				
					print ("[BULLET BEHAVIOR] --> No color found in bullet sprite and/or trails.");

				}

			}

		}

	
	}





	void OnTriggerEnter(Collider col) {

		// Register a hit.
		if ((col.gameObject.name == "Enemy Ship") && !enemyBullet) {

			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Enemy target status: Hit");
			isHit = true;

			// Calculate damage taken.
			EnemyBehavior[] enemy = col.gameObject.GetComponents<EnemyBehavior> ();
			enemy [0].ReduceHealth (damage);

			// Mark an explosion.
			yourExplosion = Instantiate (explosion) as GameObject;
			yourExplosion.transform.position = new Vector3 (transform.position.x, 
				transform.position.y, 
				transform.position.z);
			yourExplosion.name = "BOOM (SHOT BY PLAYER)";
			yourExplosion.transform.parent = col.gameObject.transform;

			// Update hit ratio.
			HitRate.RaiseHitCount();

		} else if ((col.gameObject.name == Ship.GetName ()) && enemyBullet) {
		
			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> You have been hit!");
			isHit = true;

			// Calculate damage taken.
			ShipBehavior[] ship = col.gameObject.GetComponents<ShipBehavior> ();
			ship [0].ReduceHealth (damage);

			// Mark an explosion.
			yourExplosion = Instantiate (explosion) as GameObject;
			yourExplosion.transform.position = new Vector3 (transform.position.x, 
				transform.position.y, 
				transform.position.z);
			yourExplosion.name = "BOOM (SHOT BY AI)";
			yourExplosion.transform.parent = col.gameObject.transform;

		} else if ((col.gameObject.name == Ship.GetWingmanName ()) && enemyBullet) {

			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> You have been hit!");
			isHit = true;

			// Calculate damage taken.
			ShipBehavior[] ship = col.gameObject.GetComponents<ShipBehavior> ();
			ship [0].ReduceHealth (damage);

			// Mark an explosion.
			yourExplosion = Instantiate (explosion) as GameObject;
			yourExplosion.transform.position = new Vector3 (transform.position.x, 
				transform.position.y, 
				transform.position.z);
			yourExplosion.name = "BOOM (SHOT BY AI to WINGMAN)";
			yourExplosion.transform.parent = col.gameObject.transform;

		} else if ((col.gameObject.name == "Medic Ship") && !enemyBullet) {

			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Enemy target status: Hit (medic)");
			isHit = true;

			// Calculate damage taken.
			EnemyBehavior[] enemy = col.gameObject.GetComponents<EnemyBehavior> ();
			enemy [0].ReduceHealth (damage);

			// Mark an explosion.
			yourExplosion = Instantiate (explosion) as GameObject;
			yourExplosion.transform.position = new Vector3 (transform.position.x, 
				transform.position.y, 
				transform.position.z);
			yourExplosion.name = "BOOM MEDIC! (SHOT BY PLAYER)";
			yourExplosion.transform.parent = col.gameObject.transform;

			// Update hit ratio.
			HitRate.RaiseHitCount();

		}

	}





	void OnTriggerExit(Collider col) {

		// Identify first whether it is you or the enemy ship. Penetration damage effect applied to
		// enemy ship when a player fires anti-material rounds.
		if ((col.gameObject.name == "Enemy Ship") && !enemyBullet) {
		
			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Bullet exiting from the enemy ship...");

			// Summon the behavior component.
			EnemyBehavior[] enemy = col.gameObject.GetComponents<EnemyBehavior> ();

			// Bullet will penetrate only if a ship is destroyed...
			if (!enemy [0].IsShipDestroyed()) {

				velocity = 0;
				isPenetrationFailed = true;

			}

			// Or, as long as this type of bullet can penetrate it or not.
			if (!isPenetrable) {
			
				velocity = 0;
			
			} else if(isPenetrable) {

				// Update hit ratio.
				HitRate.RaiseShotCount();
			
			}

		} else if ((col.gameObject.name == Ship.GetName ()) && enemyBullet) {
		
			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Your ship has been hit!");

			// Summon the behavior component.
			ShipBehavior[] ship = col.gameObject.GetComponents<ShipBehavior> ();

			// Enemy bullets stopped here.
			velocity = 0;

		} else if ((col.gameObject.name == Ship.GetWingmanName ()) && enemyBullet) {

			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Your ship has been hit!");

			// Summon the behavior component.
			ShipBehavior[] ship = col.gameObject.GetComponents<ShipBehavior> ();

			// Enemy bullets stopped here.
			velocity = 0;

		} else if ((col.gameObject.name == "Medic Ship") && !enemyBullet) {

			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Bullet exiting from the enemy ship...");

			// Summon the behavior component.
			EnemyBehavior[] enemy = col.gameObject.GetComponents<EnemyBehavior> ();

			// Bullet will penetrate only if a ship is destroyed...
			if (!enemy [0].IsShipDestroyed()) {

				velocity = 0;
				isPenetrationFailed = true;

			}

			// Or, as long as this type of bullet can penetrate it or not.
			if (!isPenetrable) {

				velocity = 0;

			} else if(isPenetrable) {

				// Update hit ratio.
				HitRate.RaiseShotCount();

			}

		}

	}





	void OnBecameInvisible() {

		// Bullets gone when off the screen.
//		if(enemyBullet) {
//
//			Ship.EnemyBulletCount++;
//			print ("[ BULLET BEHAVIOR ] ---> Bullet gone off screen. " + Ship.EnemyBulletCount);
//
//		}

		Destroy (gameObject);
		
	}





}
