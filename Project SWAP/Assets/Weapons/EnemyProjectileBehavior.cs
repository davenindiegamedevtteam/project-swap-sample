﻿using UnityEngine;
using System.Collections;

public class EnemyProjectileBehavior : MonoBehaviour {





	// Fire Rate per Second
	public float fireAtDistance = 4.5f; // --> Starting range of the enemy ship where fired at Z-coordinate.
	public float fireRate = 1;
	public float startFire = 0;
	public float endFire = 0;
	public int shots = 0;
	public float shotOffsetX = 0;
	public float shotOffsetY = 0;

	// Bullet/Ammo Type
	public GameObject projectileObject;
	private GameObject yourProjectTile;

	// Misc
	public bool showDebugLog = false;
	private bool isFiring;

	// SFX
	public FireSoundController gunFire;





	void Start () {
	
		isFiring = true;

	}





	void FixedUpdate () {

		if(isFiring) PullTrigger ();

	}





	void PullTrigger() {

		// Run timer.
		startFire = Time.time;

		// Fire bullet per no. of seconds.
		if((startFire > endFire) && (transform.position.z <= fireAtDistance) &&
			EnemyBehavior.IsShipDestroyedOnSpot()) {

			shots += 1;

			if(showDebugLog) {

				print("[ ENEMY PROJECTILE BEHAVIOR ] ---> No. of shots per second: " + 
					  shots + " bullet(s)/shell(s) at " + startFire + "second(s)");

			}

			gunFire.Play ();

			yourProjectTile = Instantiate(projectileObject) as GameObject;
			yourProjectTile.transform.position = new Vector3(transform.position.x + shotOffsetX,
															 transform.position.y + shotOffsetY,
															 transform.position.z - 1f);
			BulletBehavior[] properties = yourProjectTile.GetComponents<BulletBehavior> ();
			properties [0].isReverse = true;

			endFire = fireRate + Time.time;
			startFire = Time.time;

		}

	}





	public void StopFiring() {

		isFiring = false;

	}





}
