﻿using UnityEngine;
using System.Collections;

public class ExplodeDuration : MonoBehaviour {





	// Explode Properties
	public float shard = 1.5f; // --> Size of explosion.
	public float muzzleSize = 4;
	public float fade = 0.25f;
	public float muzzleFade = 0.25f;
	public Sprite flash;
	public bool showDebugLog = false;
	public bool isExplosionExpired = false;

	// Muzzle Position
	public float muzzleOffsetX = 0,
				 muzzleOffsetY = 0,
				 muzzleOffsetZ = 0;

	// SFX
	public AudioSource hit;





	void Start () {

		ExplosionUpdate ();
	
	}





	void Update () {
		
		if (!hit.isPlaying && isExplosionExpired) {
		
			Destroy (gameObject);

		}

		if (shard > 0) {

			shard -= fade;

		} else {

			shard = 0;
			isExplosionExpired = true;

		}

		transform.localScale = new Vector3 (shard, shard, shard);
	
	}





	void ExplosionUpdate () {

		// Set explosion flash by default.
		SpriteRenderer[] flash = gameObject.GetComponents<SpriteRenderer>();
		hit.Play ();
		flash[0].sprite = this.flash;

	}





}
