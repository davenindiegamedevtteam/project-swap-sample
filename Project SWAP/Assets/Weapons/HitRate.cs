﻿using UnityEngine;
using System.Collections;

public class HitRate : MonoBehaviour {





	private static float shotFired;
	private static float shotHit;





	public static void Reset() {
	
		shotFired = 0;
		shotHit = 0;

	}





	public static void RaiseShotCount() {

		shotFired++;

	}





	public static void RaiseHitCount() {

		shotHit++;

	}





	public static float GetResult() {

		float hitRate = 0;
	
		if (shotFired <= 0) {

			return PlayerPrefs.GetFloat (DataFile.HIT_RATE_DATA, 0);

		} else {
		
			hitRate = (shotHit / shotFired) * 100f;
			PlayerPrefs.SetFloat (DataFile.HIT_RATE_DATA, hitRate);
			return hitRate;

		}

	}





}
